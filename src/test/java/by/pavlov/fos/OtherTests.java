package by.pavlov.fos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RunWith(JUnit4.class)
public class OtherTests {

    @Test
    public void timeTest(){
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(1594573733000L);

        Date date = new Date(1594573733000L);

        DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy HH:mm");

        System.out.println(dateFormat.format(calendar.getTime()));
        System.out.println(dateFormat.format(date.getTime()));
        System.out.println("some");
    }

    @Test
    public void splitCoordinates(){
        String corIncome = "53.896651435726824 : 27.55396842956543,53.9067652479072 : 27.591390609741214,53.896550285240444 : 27.6097583770752";
        //TODO: remove
        String[] splittedPairs = corIncome.split(",");
        List<String> preparedCoordinates = new ArrayList<>();
        for (String cor: splittedPairs) {
            preparedCoordinates.add(cor.replace(':', ','));
        }
        preparedCoordinates.toString();

    }

    @Test
    public void parseDate() throws ParseException {
        String inputDate = "2020-07-14T19:09";

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        calendar.setTime(format.parse(inputDate));
        DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy HH:mm");

        System.out.println(dateFormat.format(calendar.getTime()));

        return;
    }


}
