package by.pavlov.fos;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SecurityTest {


    @Test
    public void userMustHavePermission(){
        UserAccount testUser = new UserAccount();
        testUser.setId(1L);
        testUser.setEmail("blabla@mail.ru");
        testUser.setName("test");
        testUser.setLastName("testovich");
        testUser.setPassword("123");
        testUser.setPhone("+111111111");
        testUser.setActive(true);
        testUser.setWalletId(1L);
        testUser.getUserRoles().add(UserRole.CLIENT);

        boolean actual = SecurityContext.getInstance().canExecute(testUser,CommandType.CREATE_FREIGHT_REQUEST);
        Assert.assertTrue(actual);

    }



}
