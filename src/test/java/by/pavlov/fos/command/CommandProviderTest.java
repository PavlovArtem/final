package by.pavlov.fos.command;

import by.pavlov.fos.command.impl.IndexCommand;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CommandProviderTest {

    private static CommandProvider commandProvider;

    @BeforeClass
    public static void init(){
        commandProvider = new BasicCommandProvider();

        commandProvider.register(CommandType.INDEX_COMMAND, new IndexCommand());
    }

    @Test
    public void shouldReturnCommand(){

    }

}
