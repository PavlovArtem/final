package by.pavlov.fos.dao;

import by.pavlov.fos.TestUtil;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import by.pavlov.fos.entity.Wallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.sql.SQLException;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SQLDAOTest {

    private final Logger LOGGER = LogManager.getLogger(SQLDAOTest.class);


    private static ApplicationContext applicationContext;


    @BeforeClass
    public static void initDb() throws SQLException {

        TestUtil.createDbTables();

    }

    @Before
    public void insertDefaults() throws SQLException {

        TestUtil.insertTestData();
    }


    @Test
    public void walletDaoTest() throws DAOException {

        WalletDAO walletDAO = ApplicationContext.getInstance().getBean(WalletDAO.class);

        Wallet wallet = new Wallet();
        wallet.setAmount(new BigDecimal(0));
        Long id = walletDAO.save(wallet);

        assertNotNull(walletDAO);
    }

    @Test
    public void userAccountDAOTest() throws DAOException{

        UserAccDAO userAccDAO = ApplicationContext.getInstance().getBean(UserAccDAO.class);
        WalletDAO walletDAO = ApplicationContext.getInstance().getBean(WalletDAO.class);

        Wallet wallet = new Wallet();
        wallet.setAmount(new BigDecimal(0));
        Long walletId = walletDAO.save(wallet);

        UserAccount userAccount = new UserAccount();
        userAccount.setEmail("test@test.ru");
        userAccount.setName("Vasya");
        userAccount.setLastName("Testovich");
        userAccount.setPhone("1235467");
        userAccount.setPassword("123");
        userAccount.setActive(true);
        userAccount.setWalletId(walletId);
        userAccount.getUserRoles().add(UserRole.CLIENT);
        Long id = userAccDAO.save(userAccount);

        LOGGER.info(userAccount.getWalletId());
        assertNotNull(id);

    }


    @After
    public void clear() throws SQLException {
        TestUtil.clearUpTestDb();
    }









}
