package by.pavlov.fos.service;

import by.pavlov.fos.TestUtil;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.SQLException;
import java.util.Optional;

@RunWith(JUnit4.class)
public class UserAccServiceTest {

    private static final Logger LOGGER = LogManager.getLogger(UserAccServiceTest.class);

    @BeforeClass
    public static void initTestDb() throws SQLException {
        TestUtil.createDbTables();

    }

    @Before
    public void insertDefaults() throws SQLException {

        TestUtil.insertTestData();
    }

    @After
    public void clearTestDb() throws SQLException {

        TestUtil.clearUpTestDb();
    }


    @Test
    public void saveUserTest() throws ServiceException {
        UserAccountService userAccountService = ApplicationContext.getInstance().getBean(UserAccountService.class);

        String userEmail = "test@test.ru";

        UserAccount userAccount = new UserAccount();
        userAccount.setEmail(userEmail);
        userAccount.setName("Vasya");
        userAccount.setLastName("Testovich");
        userAccount.setPhone("1235467");
        userAccount.setPassword("123");
        userAccount.setActive(true);
        userAccount.getUserRoles().add(UserRole.CLIENT);

        boolean isSuccess = userAccountService.signUp(userAccount);
        UserAccount receivedUser = userAccountService.findByEmail(userEmail).get();
        Assert.assertEquals(userAccount, receivedUser);
    }



}
