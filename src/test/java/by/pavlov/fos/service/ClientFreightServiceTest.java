package by.pavlov.fos.service;

import by.pavlov.fos.TestUtil;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dto.DriverOfferDto;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.entity.FreightRequest;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@RunWith(JUnit4.class)
public class ClientFreightServiceTest {

    private final ClientFreightService clientFreightService = ApplicationContext.getInstance().getBean(ClientFreightService.class);

    @BeforeClass
    public static void initTestDb() throws SQLException {

        TestUtil.createDbTables();
    }

    @Before
    public void insertDefaults() throws SQLException {

        TestUtil.insertTestData();
    }

    @After
    public void clear() throws SQLException {
        TestUtil.clearUpTestDb();
    }


    @Test
    public void createFreightRequestTest() throws DAOException {

        FreightRequestDto freightRequestDto = new FreightRequestDto();
        freightRequestDto.setCargoWeight(120D);
        freightRequestDto.setCargoVolume(10D);
        freightRequestDto.setFreightDate(Calendar.getInstance());
        freightRequestDto.setUserId(1L);
        freightRequestDto.setDescription("");
        freightRequestDto.setRequestStatus(FreightRequest.RequestStatus.OPEN);
        freightRequestDto.setCoordinates(Arrays.asList("53.896651435726824 , 27.55396842956543", "53.85454654 , 27.896554547"));
        Long id = clientFreightService.createFreightRequest(freightRequestDto);
        freightRequestDto.setId(id);

        FreightRequestDto expectedDto = clientFreightService.getFreightRequest(id);

        Assert.assertEquals(expectedDto, freightRequestDto);


    }




}
