package by.pavlov.fos.service;

import by.pavlov.fos.TestUtil;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.dao.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class WalletServiceTest {

    private static final Logger LOGGER = LogManager.getLogger(WalletServiceTest.class);
    private final WalletService walletService = ApplicationContext.getInstance().getBean(WalletService.class);
    private final Long defaultWalletId = 1L;

    @BeforeClass
    public static void initTestDb() throws SQLException {

        TestUtil.createDbTables();
    }

    @Before
    public void insertDefaults() throws SQLException {

        TestUtil.insertTestData();
    }

    @After
    public void truncateDb() throws SQLException {
        TestUtil.clearUpTestDb();
    }

    @Test
    public void walletServiceGetTest(){

        try {
            Assert.assertNotNull(walletService.getWallet(defaultWalletId));
        } catch (DAOException e) {
            LOGGER.error("error occurred while trying to get wallet ",e);
        }

    }

    @Test
    public void walletServiceIsEnoughMoney(){

        try {
            BigDecimal checkAmount = BigDecimal.valueOf(200L);
            Assert.assertFalse(walletService.isEnoughMoney(checkAmount, defaultWalletId));

        } catch (DAOException e) {
            LOGGER.error("error occurred while trying to check wallet amount ",e);
        }

    }


    @Test
    public void walletRefillTest(){

        try {
            BigDecimal checkAmount = BigDecimal.valueOf(200L);
            Assert.assertFalse(walletService.isEnoughMoney(checkAmount, defaultWalletId));
            Assert.assertTrue(walletService.fillUpWallet(checkAmount, defaultWalletId));
            Assert.assertTrue(walletService.isEnoughMoney(checkAmount, defaultWalletId));

        } catch (DAOException e) {
            LOGGER.error("error occurred while trying to check wallet amount ",e);
        }
    }





}
