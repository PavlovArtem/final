package by.pavlov.fos.validation;

import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class ValidationTest {

    private EntityValidator entityValidator;

    @Before
    public void init(){
        entityValidator = ApplicationContext.getInstance().getBean(EntityValidator.class);
    }

    @Test
    public void shouldPassValidation(){

        UserAccount userAccount = new UserAccount();

        userAccount.setId(1L);
        userAccount.setName("Han");
        userAccount.setLastName("Solo");
        userAccount.setPhone("+12345678");
        userAccount.setEmail("chubaka@gmail.com");
        userAccount.setUserRoles(Collections.singletonList(UserRole.CLIENT));
        userAccount.setActive(true);
        userAccount.setWalletId(1L);
        userAccount.setPassword("123");

        ValidationResult validationResult = entityValidator.validate(userAccount);
        Assert.assertNotNull(validationResult);
        Assert.assertTrue(validationResult.getBrokenFields().isEmpty());
    }


    @Test
    public void shouldFailOnEmail(){

        UserAccount userAccount = new UserAccount();

        userAccount.setId(1L);
        userAccount.setName("Han");
        userAccount.setLastName("Solo");
        userAccount.setPhone("+12345678");
        userAccount.setEmail("chubaka@.vom");
        userAccount.setUserRoles(Collections.singletonList(UserRole.CLIENT));
        userAccount.setActive(true);
        userAccount.setWalletId(1L);
        userAccount.setPassword("123");

        ValidationResult validationResult = entityValidator.validate(userAccount);
        Assert.assertNotNull(validationResult);
        List<BrokenField> brokenFields = validationResult.getBrokenFields();
        BrokenField brokenField = brokenFields.get(0);
        Assert.assertEquals("email",brokenField.getValidationRule());
        Assert.assertEquals("chubaka@.vom",brokenField.getFieldValue());
        Assert.assertEquals("email",brokenField.getFieldName());



    }


}
