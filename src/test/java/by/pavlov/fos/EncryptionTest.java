package by.pavlov.fos;

import by.pavlov.fos.utils.BasicPasswordEncryptionHandler;
import by.pavlov.fos.utils.PasswordEncryptionHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

@RunWith(JUnit4.class)
public class EncryptionTest {

    @Test
    public void encryptionTest() throws NoSuchAlgorithmException {

        String password = "8594562afd$A<";
        String mustBeEquals = "8594562afd$A<";

        PasswordEncryptionHandler encryptionHandler = new BasicPasswordEncryptionHandler();

        String result1 = encryptionHandler.encryptPassword(password);
        String result2 = encryptionHandler.encryptPassword(mustBeEquals);

        Assert.assertEquals(result1,result2);

    }




}
