var rootContext = "${pageContext.request.contextPath}static/leaflet/start.png";
var map = L.map('map').setView([53.893009, 27.567444], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'
}).addTo(map);


