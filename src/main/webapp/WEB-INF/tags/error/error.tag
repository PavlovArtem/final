<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${not empty requestScope.errorMessages}">


    <article class="message is-danger" id="message">
        <div class="message-header">
            <strong><fmt:message key="error.notification"/></strong>

            <button class="delete" id="delete" onclick="hideMessage()" aria-label="delete"></button>
        </div>
        <div class="message-body">
            <ul>
                <c:forEach var="error" items="${requestScope.errorMessages}">
                    <li>
                        <c:out value="${error}"/>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </article>

    <script>
        let message = document.getElementById('message');
        let messageCloseBtn = document.getElementById('delete');
        function hideMessage () {
            message.style.display = 'none';
        }
    </script>


</c:if>
