<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty param.isSuccess}">

    <article class="message is-success" id="message">
        <div class="message-header">
            <strong><fmt:message key="success.notification"/></strong>
            <button class="delete" id="delete" onclick="hideMessage()" aria-label="delete"></button>
        </div>
        <div class="message-body">
           <c:out value="${param.isSuccess}"/>
        </div>

    </article>

    <script>
        let message = document.getElementById('message');
        let messageCloseBtn = document.getElementById('delete');
        function hideMessage () {
            message.style.display = 'none';
        }
    </script>

</c:if>
