<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/21/2020
  Time: 12:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>

<div class="container">
    <div class="notification">
        <c:if test="${not empty requestScope.isSuccess}">
            <strong><c:out value="${requestScope.isSuccess}"/> </strong>
        </c:if>
        <strong><fmt:message key="user.wallet.info.amount"/></strong>
        <strong><c:out value="${requestScope.wallet.amount}"/></strong><br/>
        <div class="content">
            <form action="" method="post">
                <label><fmt:message key="user.wallet.fill.amount"/>
                    <input class="input is-primary" type="number" name="amount">
                </label>
                <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}" value="${CommandType.FILL_UP_WALLET_COMMAND}">
                <button class="button is-primary"><fmt:message key="user.wallet.fill"/></button>
            </form>
        </div>
    </div>
</div>



