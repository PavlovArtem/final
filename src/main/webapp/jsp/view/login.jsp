<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/7/2020
  Time: 7:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>

<div class="container">
    <div class="column is-centered">
        <div column is-5-tablet is-4-desctop is-3-widescreen>
            <form method="post" action="${pageContext.request.contextPath}/" class="box">
                <div class="field">
                    <label> <fmt:message key="user.email"/>
                        <input class="input is-primary is-transparent" type="email" name="uemail" placeholder="Email"
                               onfocus="this.value=''" required>
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="user.password"/>
                        <input class="input is-primary is-transparent" type="password" name="upassword" placeholder="password"
                               onclick="this.value=''" required>
                    </label>
                </div>
                <button class="button is-primary is-transparent" type="submit" value="register"><fmt:message key="links.user.login"/> </button>
                <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}" value="${CommandType.LOGIN_COMMAND}">
            </form>
        </div>
    </div>
</div>

