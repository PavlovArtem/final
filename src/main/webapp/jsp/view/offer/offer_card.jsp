<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/20/2020
  Time: 4:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>


<style>
    .image-preview{
        width: 300px;
        height: 150px;
        border: 2px solid #dddddd;
        margin-top: 15px;

        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: bold;
        color: #cccccc;
    }
    .image-preview__image{
        height: 100%;
        width: 100%;
    }
</style>
<div class="container">
    <div class="image-preview" id="image-preview">
        <img src="data:image/jpg;base64,${requestScope.display_offer_dto.transportEncodedPhoto}" alt="No image"  class="image-preview__image" id="transport-image" >
    </div>
    <b><fmt:message key="offer.price"/> <c:out value="${requestScope.display_offer_dto.price}"/></b><br/>
    <b><fmt:message key="freight.request.cargo.volume"/>  <c:out value="${requestScope.display_offer_dto.description}"/></b><br/>
</div>
