<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/19/2020
  Time: 1:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>

<style>

    #transport_list ul {
        list-style: none;
    }

    #transport_list li {
        display: inline;
    }

</style>
<div class="columns">
    <div class="column is-left is-half">
        <div class="container">

            <c:if test="${not empty requestScope.freight_request_list}">
                <table class="table is-striped">
                    <thead>
                    <tr>
                        <th><fmt:message key="request.offers.requests"/></th>
                        <th><fmt:message key="request.offers.offers"/></th>
                    </tr>

                    </thead>
                    <tbody>
                    <c:forEach var="freight_dto" items="${requestScope.freight_request_list}">
                        <tr>
                            <td><c:set var="freight_dto" value="${freight_dto}" scope="request"/>
                                <jsp:include page="../request/freight_card.jsp"/>
                                <button class="button is-primary"
                                        onclick="showModal(${freight_dto.id}, ${freight_dto.coordinates})"><fmt:message
                                        key="request.accept"/></button>
                            </td>
                            <td id="request_id${freight_dto.id}">

                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </c:if>
        </div>
    </div>
    <div class="column is-right is-half">
        <strong><fmt:message key="request.offers.offers"/></strong>

    </div>
    <%--    <button id="myBtn">Open Modal</button>--%>
    <div id="myModal" class="modal">
        <div class="modal-background" onclick="closeModal()"></div>
        <!-- Modal content -->
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">Modal title</p>
                <button class="delete" id="close" onclick="closeModal()" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div id="map" class="map" style="height: 400px; width: auto; z-index: 2;"></div>

                <div class="transport_to_chose">
                    <div class="transport_ul" id="transport_list">
                        <c:if test="${not empty requestScope.transport_dto_list}">
                            <ul>
                                <c:forEach var="transport_dto" items="${requestScope.transport_dto_list}">
                                    <li>
                                        <c:set var="transport_dto" value="${transport_dto}" scope="request"/>
                                        <jsp:include page="../transport/transport_card.jsp"/>
                                        <input type="radio" name="offer_transport_id" id="offer_transport_id"
                                               value="${transport_dto.id}" form="offer_form">
                                    </li>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </div>
                </div>
                <form action="${pageContext.request.contextPath}/" method="post" id="offer_form">
                    <label> <fmt:message key="offer.price"/>
                        <input class="input is-primary" type="number" name="offer_price">
                    </label>
                    <label> <fmt:message key="offer.description"/>
                        <input class="input is-primary" type="text" name="offer_description">
                    </label>
                    <input type="hidden" id="offer_freight_id" name="freight_id">
                    <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                           value="${CommandType.CREATE_DRIVER_OFFER_COMMAND}">
                    <button class="button is-primary" type="submit" onclick="sendOfferAsync(this.form)">Send offer
                    </button>
                </form>
            </section>
            <footer class="modal-card-foot">

            </footer>
        </div>
    </div>

</div>

<script>
    "use strict";

    let initMapVal = false;

    let rootContext = "${pageContext.request.contextPath}static/leaflet/start.png";
    let map;
    let startIcon;
    let routeControl;


    map = L.map('map').setView([53.893009, 27.567444], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'
    }).addTo(map);

    startIcon = L.icon({
        iconUrl: rootContext,
        iconSize: [40, 60], // size of the icon
        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    routeControl = L.Routing.control({
        router: L.Routing.mapbox('pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'),
        draggable: false
    }).addTo(map);


    function GetURLParameter(sParam) {
        let sPageURL = window.location.search.substring(1);
        let sURLVariables = sPageURL.split('&');
        for (let i = 0; i < sURLVariables.length; i++) {
            let sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1];
            }
        }
    }


    function sendOfferAsync(form) {
        let urlPost = form.attr("action");
        let getUrl = '${pageContext.request.contextPath}/?${ApplicationConstants.COMMAND_PARAM}=${CommandType.OFFER_GET_COMMAND}';
        document.ready(function () {
            $('#submit').click(function (event) {
                let request = $.ajax({
                    method: 'POST',
                    url: urlPost,
                    data: form.serialize()
                });
                request.done(function (isSuccess) {
                    console.log(isSuccess);
                });
                let getOffer = $.ajax({
                    method: 'GET',
                    url: getUrl,
                    data: {
                        'freight_id': inputFreightId.getAttribute('value')
                    },
                    success: function () {
                        console.log(response);
                    }

                });
            });
        });

    }


    // Get the modal
    let modal = document.getElementById("myModal");

    let btn = document.getElementById("myBtn");

    let inputFreightId = document.getElementById("offer_freight_id");

    // Get the <span> element that closes the modal
    let closeModalBtn = document.getElementById("close");

    // When the user clicks the button, open the modal
    function showModal(id, cors) {


        map.setView([53.893009, 27.567444], 13);

        let pointsCounter = 0;
        let latVal = 0;
        let lngVal = 0;

        for (let i = 0; i < cors.length; i++) {

            if ((i % 2) !== 0) {
                console.log(i);
                lngVal = cors[i];

                if (pointsCounter < 1) {
                    routeControl.spliceWaypoints(0, 1, L.latLng(latVal, lngVal));
                    ++pointsCounter;
                } else if (pointsCounter < 2) {
                    routeControl.spliceWaypoints(routeControl.getWaypoints().length - 1, 1, L.latLng(latVal, lngVal));
                    ++pointsCounter;
                } else {
                    let wayPointsLength = routeControl.getWaypoints().length;
                    routeControl.spliceWaypoints(wayPointsLength, wayPointsLength, L.latLng(latVal, lngVal));
                    ++pointsCounter;
                }

                latVal = 0;
                lngVal = 0;

            } else {
                latVal = cors[i];
            }

        }

        modal.classList.add('is-active');
        modal.classList.add('is-clipped');

        inputFreightId.setAttribute('value', id);
    }

    function closeModal() {
        routeControl.getPlan().setWaypoints();
        modal.classList.remove('is-clipped');
        modal.classList.remove('is-active');
    }

</script>

</div>
