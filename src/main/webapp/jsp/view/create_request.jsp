<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/11/2020
  Time: 10:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags" %>


<div class="container is-fullheight">
    <div class="container">
        <div class="box">
            <div class="column ">
                <div id="map" class="map" style="height: 400px; width: auto; z-index: 2;"></div>
                <script>

                    let rootContext = "${pageContext.request.contextPath}static/leaflet/start.png";
                    let map = L.map('map').setView([53.893009, 27.567444], 13);
                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'mapbox/streets-v11',
                        tileSize: 512,
                        zoomOffset: -1,
                        accessToken: 'pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'
                    }).addTo(map);

                    let startIcon = L.icon({
                        iconUrl: rootContext,
                        iconSize: [40, 60], // size of the icon
                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });

                    let routeControl = L.Routing.control({
                        router: L.Routing.mapbox('pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'),
                        draggable: false
                    }).addTo(map);


                    let pointsCounter = 0;
                    let popup = L.popup();
                    let markers = [];


                    function createButton(label, container) {
                        var btn = L.DomUtil.create('button', '', container);
                        btn.setAttribute('type', 'button');
                        btn.innerHTML = label;
                        return btn;
                    }

                    map.on('click', function (e) {
                        let container = L.DomUtil.create('div'),
                            addPointBtn = createButton('Add point', container);

                        L.popup()
                            .setContent(container)
                            .setLatLng(e.latlng)
                            .openOn(map);


                        L.DomEvent.on(addPointBtn, 'click', function () {

                            if (pointsCounter < 1) {
                                routeControl.spliceWaypoints(0, 1, e.latlng);
                                //alert(JSON.stringify(routeControl.getWaypoints()));
                                map.closePopup();
                                ++pointsCounter;
                            } else if (pointsCounter < 2) {
                                routeControl.spliceWaypoints(routeControl.getWaypoints().length - 1, 1, e.latlng);
                                map.closePopup();
                                ++pointsCounter;
                            } else {
                                let wayPointsLength = routeControl.getWaypoints().length;
                                routeControl.spliceWaypoints(wayPointsLength, wayPointsLength, e.latlng);
                                map.closePopup();
                                ++pointsCounter;
                            }

                        });
                    });


                    // function onMapClick(e) {
                    //
                    //     let marker = L.marker([e.latlng.lat, e.latlng.lng]);
                    //     marker.addTo(routeControl.waypoints);
                    //     routeControl.waypoints.push(L.latLng(marker._latlng.lat, marker._latlng.lng));
                    //     marker.bindPopup("To delete mouse double click");
                    //     marker.on('mouseover', function (e) {
                    //         this.openPopup();
                    //     });
                    //     marker.on('mouseout', function (e) {
                    //         this.closePopup();
                    //     });
                    //     marker.on('dblclick', function (e) {
                    //         let i = markers.indexOf(marker);
                    //         markers.splice(i, 1);
                    //         marker.remove();
                    //     });
                    //     addPointToRoute(marker);
                    //     markers.push(marker);
                    // }
                    //
                    // map.on('click', onMapClick);


                    function sendRoutePoints() {
                        let coordinates = [];
                        let routePoints = routeControl.getWaypoints();
                        for (let i = 0; i < routePoints.length; ++i) {

                            if (routePoints[i] instanceof L.Routing.Waypoint) {
                                coordinates.push(routePoints[i].latLng.lat + ' : ' + routePoints[i].latLng.lng);
                            }
                        }
                        document.getElementById("${ApplicationConstants.FREIGHT_ROUTE_POINTS}").value = coordinates;
                        document.getElementById("request_form").submit();
                    }
                </script>

            </div>
            <div class="column is-left">
                <form method="post" action="" id="request_form">
                    <div class="field">
                        <label> <fmt:message key="freight.request.cargo.weight"/>
                            <input class="input is-primary" type="number" name="${ApplicationConstants.CARGO_WEIGHT}"
                                   required>
                        </label>
                    </div>

                    <div class="field">
                        <label> <fmt:message key="freight.request.cargo.volume"/>
                            <input class="input is-primary" type="number" name="${ApplicationConstants.CARGO_VOLUME}"
                                   required>
                        </label>
                    </div>

                    <div class="field"><label> <fmt:message key="freight.request.cargo.datetime"/>
                        <input class="input is-primary" type="datetime-local"
                               name="${ApplicationConstants.FREIGHT_DATE}" required>
                    </label></div>

                    <div class="field">
                        <label> <fmt:message key="freight.request.cargo.description"/>
                            <input class="input is-primary" type="text" name="description">
                        </label>
                    </div>

                    <input type="hidden" id="<c:out value='${ApplicationConstants.FREIGHT_ROUTE_POINTS}'/>"
                           name="${ApplicationConstants.FREIGHT_ROUTE_POINTS}">
                    <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                           value="${CommandType.CREATE_FREIGHT_REQUEST}">

                    <button onclick="sendRoutePoints()" class="button is-primary"
                            id="${ApplicationConstants.PLACE_REQUEST}">Place request
                    </button>
                </form>
            </div>
        </div>
    </div>

</div>
