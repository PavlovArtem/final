<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/18/2020
  Time: 5:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>


<style>
    .image-preview {
        width: 300px;
        min-height: 150px;
        border: 2px solid #dddddd;
        margin-top: 15px;

        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: bold;
        color: #cccccc;
    }

    .image-preview__image {
        display: none;
        width: 100%;
    }
</style>
<div class="container is-fullheight">
    <div class="container">
        <div class="column is-center">
            <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data" class="box">
                <div class="file has-name">
                   <span class="file-cta">
      <span class="file-icon">
        <i class="fas fa-upload"></i>
      </span>
      <span class="file-label">
        <fmt:message key="transport.create.image"/>
      </span>
    </span>

                    <input required class="file-input" type="file" name="${ApplicationConstants.TRANSPORT_IMAGE}"
                           id="transport_image">

                </div>
                <div class="image-preview" id="image-preview">
                    <img src="" alt="Image preview" class="image-preview__image">
                    <span class="image-preview__default-text">Image preview</span>
                </div>
                <div class="field">
                    <label> <fmt:message key="transport.create.name"/>
                        <input required class="input is-primary" type="text"
                               name="${ApplicationConstants.TRANSPORT_NAME}"><br/>
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="transport.create.max.weight"/>
                        <input required class="input is-primary" type="number"
                               name="${ApplicationConstants.TRANSPORT_MAX_WEIGHT}"><br/>
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="transport.create.max.volume"/>
                        <input required class="input is-primary" type="text"
                               name="${ApplicationConstants.TRANSPORT_MAX_VOLUME}"><br/>
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="transport.create.description"/>
                        <input required class="input is-primary" type="text"
                               name="${ApplicationConstants.TRANSPORT_DESCRIPTION}"><br/>
                    </label>
                </div>
                <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                       value="${CommandType.CREATE_TRANSPORT_COMMAND}">
                <button class="button is-primary" type="submit" name="${ApplicationConstants.COMMAND_PARAM}"
                        value="${CommandType.CREATE_TRANSPORT_COMMAND}"> ><fmt:message key="transport.create"/></button>
            </form>
        </div>
    </div>

    <script>
        const inpFile = document.getElementById("transport_image");
        const previewContainer = document.getElementById("image-preview");
        const previewImage = previewContainer.querySelector(".image-preview__image");
        const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

        inpFile.addEventListener("change", function () {
            const file = this.files[0];

            if (file) {
                const reader = new FileReader();

                previewDefaultText.style.display = "none";
                previewImage.style.display = "block";

                reader.addEventListener("load", function () {
                    previewImage.setAttribute("src", this.result);

                });
                reader.readAsDataURL(file);
            } else {
                previewDefaultText.style.display = null;
                previewImage.style.display = null;
                previewImage.setAttribute("src", "");
            }
        });

    </script>
</div>

