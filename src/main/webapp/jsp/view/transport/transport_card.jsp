<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/18/2020
  Time: 7:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
    .image-preview {
        width: 300px;
        height: 150px;
        border: 2px solid #dddddd;
        margin-top: 15px;

        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: bold;
        color: #cccccc;
    }

    .image-preview__image {
        height: 100%;
        width: 100%;
    }
</style>
<div class="container is-centered">
    <div class="image-preview" id="image-preview">
        <img src="data:image/jpg;base64,${requestScope.transport_dto.encodedPhoto}" alt="No image"
             class="image-preview__image" id="transport-image">
    </div>
    <strong><fmt:message key="transport.create.max.weight"/> <c:out
            value="${requestScope.transport_dto.maxWeight}"/></strong><br/>
    <strong><fmt:message key="transport.create.max.volume"/> <c:out
            value="${requestScope.transport_dto.maxVolume}"/></strong><br/>
    <strong><fmt:message key="transport.create.description"/> <c:out
            value="${requestScope.transport_dto.description}"/></strong><br/>
</div>
