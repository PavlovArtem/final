<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/18/2020
  Time: 10:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<div class="container is-centered">
    <c:choose>
        <c:when test="${not empty requestScope.transport_dto_list}">
            <ul>
                <c:forEach var="transport_dto" items="${requestScope.transport_dto_list}">
                    <li class="box">
                        <c:set var="transport_dto" value="${transport_dto}" scope="request"/>
                        <jsp:include page="transport_card.jsp"/>

                        <form method="get" action="${pageContext.request.contextPath}/">
                            <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}" value="${CommandType.TRANSPORT_DISPLAY_UPDATE_COMMAND}">
                            <input type="hidden" name="transport_id" value="<c:out value='${transport_dto.id}'/>">
                            <button class="button is-warning" >
                                <fmt:message key="transport.edit"/></button>
                        </form>

                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <div class="container is-centered has-text-centered">
                <form class="box has-text-centered" method="get">
                    <p><fmt:message key="transport.not.added.yet"/></p>
                    <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                           value="${CommandType.TRANSPORT_DISPLAY_CREATION_COMMAND}">
                    <button class="button is-primary"><fmt:message key="transport.create"/></button>
                </form>
            </div>
        </c:otherwise>
    </c:choose>

    <div id="myModal" class="modal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <strong>Update transport</strong>
                <button class="delete" id="close" onclick="closeModal()" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <jsp:useBean id="transport" scope="request" class="by.pavlov.fos.dto.ShowTransportDto"/>
                <div class="container">
                    <div class="column is-center">
                        <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data"
                              class="box">
                            <div class="file has-name">
                   <span class="file-cta">
      <span class="file-icon">
        <i class="fas fa-upload"></i>
      </span>
      <span class="file-label">
        <fmt:message key="transport.create.image"/>
      </span>
    </span>

                                <input required class="file-input" type="file"
                                       name="${ApplicationConstants.TRANSPORT_IMAGE}"
                                       id="transport_image">

                            </div>
                            <div class="image-preview" id="image-preview">
                                <img src="data:image/jpg;base64<c:out value='${transport.encodedPhoto}'/>"
                                     alt="Image preview" class="image-preview__image">
                                <span class="image-preview__default-text">Image preview</span>
                            </div>
                            <div class="field">
                                <label> <fmt:message key="transport.create.name"/>
                                    <input required class="input is-primary" type="text"
                                           value="<c:out value="${transport.name}"/>"
                                           name="${ApplicationConstants.TRANSPORT_NAME}"><br/>
                                </label>
                            </div>
                            <div class="field">
                                <label> <fmt:message key="transport.create.max.weight"/>
                                    <input required class="input is-primary" type="number"
                                           value="<c:out value="${transport.maxWeight}"/>"
                                           name="${ApplicationConstants.TRANSPORT_MAX_WEIGHT}"><br/>
                                </label>
                            </div>
                            <div class="field">
                                <label> <fmt:message key="transport.create.max.volume"/>
                                    <input required class="input is-primary" type="text"
                                           value="<c:out value="${transport.maxVolume}"/>"
                                           name="${ApplicationConstants.TRANSPORT_MAX_VOLUME}"><br/>
                                </label>
                            </div>
                            <div class="field">
                                <label> <fmt:message key="transport.create.description"/>
                                    <input required class="input is-primary" type="text"
                                           value="<c:out value="${transport.description}"/>"
                                           name="${ApplicationConstants.TRANSPORT_DESCRIPTION}"><br/>
                                </label>
                            </div>
                            <input type="hidden" name="transport_id" value="<c:out value="${transport.id}"/>">
                            <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                                   value="${CommandType.CREATE_TRANSPORT_COMMAND}">
                            <button class="button is-primary" type="submit" name="${ApplicationConstants.COMMAND_PARAM}"
                                    value="${CommandType.CREATE_TRANSPORT_COMMAND}"> ><fmt:message
                                    key="transport.create"/></button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<script>
    let modal = document.getElementById("myModal");

    let btn = document.getElementById("myBtn");

    let inputFreightId = document.getElementById("offer_freight_id");

    // Get the <span> element that closes the modal
    let closeModalBtn = document.getElementById("close");

    // When the user clicks the button, open the modal
    function showModal(transport) {
        modal.classList.add('is-active');
        modal.classList.add('is-clipped');
        console.log(transport);
        inputFreightId.setAttribute('value', id);
    }

    function closeModal() {
        modal.classList.remove('is-clipped');
        modal.classList.remove('is-active');
    }

</script>

