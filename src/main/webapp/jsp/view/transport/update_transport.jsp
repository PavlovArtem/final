<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 8/9/2020
  Time: 12:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>


<style>
    .image-preview {
        width: 300px;
        min-height: 150px;
        border: 2px solid #dddddd;
        margin-top: 15px;

        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: bold;
        color: #cccccc;
    }

    .image-preview__image {
        width: 100%;
    }
</style>
<c:choose>
    <c:when test="${not empty requestScope.transport}">
        <jsp:useBean id="transport" scope="request" class="by.pavlov.fos.dto.ShowTransportDto"/>
        <div class="container is-fullheight">
            <div class="container">
                <div class="column is-center">
                    <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data"
                          class="box">
                        <div class="file has-name">
                            <span class="file-cta">
                                 <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                    </span>
                                    <span class="file-label">
                                        <fmt:message key="transport.create.image"/>
                                    </span>
                             </span>

                            <input required class="file-input" type="file"
                                   name="${ApplicationConstants.TRANSPORT_IMAGE}"
                                   id="transport_image">
                        </div>
                        <div class="image-preview" id="image-preview">
                            <img src="data:image/jpg;base64,${transport.encodedPhoto}" alt="Image preview"
                                 class="image-preview__image">
                            <span class="image-preview__default-text">Image preview</span>
                        </div>
                        <div class="field">
                            <label> <fmt:message key="transport.create.name"/>
                                <input required class="input is-primary" type="text"
                                       value="<c:out value="${transport.name}"/>"
                                       name="${ApplicationConstants.TRANSPORT_NAME}"><br/>
                            </label>
                        </div>
                        <div class="field">
                            <label> <fmt:message key="transport.create.max.weight"/>
                                <input required class="input is-primary" type="number"
                                       value="<c:out value="${transport.maxWeight}"/>"
                                       name="${ApplicationConstants.TRANSPORT_MAX_WEIGHT}"><br/>
                            </label>
                        </div>
                        <div class="field">
                            <label> <fmt:message key="transport.create.max.volume"/>
                                <input required class="input is-primary" type="text"
                                       value="<c:out value="${transport.maxVolume}"/>"
                                       name="${ApplicationConstants.TRANSPORT_MAX_VOLUME}"><br/>
                            </label>
                        </div>
                        <div class="field">
                            <label> <fmt:message key="transport.create.description"/>
                                <input required class="input is-primary" type="text"
                                       value="<c:out value="${transport.description}"/>"
                                       name="${ApplicationConstants.TRANSPORT_DESCRIPTION}"><br/>
                            </label>
                        </div>
                        <input type="hidden" name="transport_id" value="<c:out value="${transport.id}"/>">
                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                               value="${CommandType.CREATE_TRANSPORT_COMMAND}">
                        <button class="button is-warning" type="submit" name="${ApplicationConstants.COMMAND_PARAM}"
                                value="${CommandType.CREATE_TRANSPORT_COMMAND}"><fmt:message
                                key="transport.edit"/></button>
                    </form>
                </div>
            </div>

            <script>
                const inpFile = document.getElementById("transport_image");
                const previewContainer = document.getElementById("image-preview");
                const previewImage = previewContainer.querySelector(".image-preview__image");
                const previewDefaultText = previewContainer.querySelector(".image-preview__default-text");

                inpFile.addEventListener("change", function () {
                    const file = this.files[0];

                    if (file) {
                        const reader = new FileReader();

                        // previewDefaultText.style.display = "none";
                        // previewImage.style.display = "block";

                        reader.addEventListener("load", function () {
                            previewImage.setAttribute("src", this.result);

                        });
                        reader.readAsDataURL(file);
                    } else {
                        previewDefaultText.style.display = null;
                        previewImage.style.display = null;
                        previewImage.setAttribute("src", "");
                    }
                });

            </script>
        </div>
    </c:when>
    <c:otherwise>
        <strong>Transport not found</strong>
    </c:otherwise>
</c:choose>




