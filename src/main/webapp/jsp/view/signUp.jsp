<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/7/2020
  Time: 1:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags" %>

<div class="container">
    <div class="column is-centered">
        <div class="is-5-tablet is-4-desktop is-3-widescreen">
            <form method="post" action="${pageContext.request.contextPath}/" class="box">
                <div class="field">
                    <label> <fmt:message key="user.email"/>
                        <input class="input is-primary" type="email" name="uemail" placeholder="Email" required
                               onfocus="this.value=''">
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="user.firstName"/>
                        <input class="input is-primary" type="text" name="uname" placeholder="Name"
                               onclick="this.value=''"
                               required>
                    </label>
                </div>
                <div class="field">
                    <label> delete later
                        <input class="input is-primary" type="text" name="ulastname" placeholder="Lastname" required
                               onclick="this.value=''">
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="user.phoneNumber"/>
                        <input class="input is-primary" type="text" name="uphone" placeholder="phone" required
                               onclick="this.value=''">
                    </label>
                </div>
                <div class="field">
                    <label> <fmt:message key="user.password"/>
                        <input class="input is-primary" type="password" name="upassword" placeholder="Name" required
                               onclick="this.value=''">
                    </label>
                </div>

                <div class="field">
                    <div class="control">
                        <label class="radio">
                            <input type="radio" name="urole" value="CLIENT" required>
                            <fmt:message key="user.role.client"/>
                        </label>
                        <label class="radio">
                            <input type="radio" name="urole" value="DRIVER" required>
                            <fmt:message key="user.role.driver"/>
                        </label>
                    </div>
                </div>

                <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                       value="${CommandType.SIGN_UP_COMMAND}">
                <button class="button is-primary" type="submit" value="register"><fmt:message key="links.user.sign.up"/> </button>
            </form>
        </div>
    </div>
</div>

