<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/21/2020
  Time: 8:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<style>
    .image-preview{
        width: 300px;
        height: 150px;
        border: 2px solid #dddddd;
        margin-top: 15px;

        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: bold;
        color: #cccccc;
    }
    .image-preview__image{
        height: 100%;
        width: 100%;
    }
</style>
<div class="container">
    <strong><fmt:message key="order.status"/> <c:out value="${requestScope.orderDto.orderStatus}"/> </strong><br/>
    <c:set var="freightRequest" value="${requestScope.orderDto.freightRequest}" scope="request"/>
    <strong><fmt:message key="freight.request.cargo.weight"/>  <c:out value="${requestScope.freightRequest.cargoWeight}"/></strong><br/>
    <strong><fmt:message key="freight.request.cargo.volume"/>  <c:out value="${requestScope.freightRequest.cargoVolume}"/></strong><br/>
    <strong><fmt:message key="freight.request.cargo.datetime"/> <fmt:formatDate
            value="${requestScope.freightRequest.freightDate.time}" type="both" dateStyle="short"
            timeStyle="medium"/> </strong><br/>
    <c:set var="driverOffer" value="${requestScope.orderDto.driverOffer}" scope="request"/>
    <strong><fmt:message key="offer.price"/> <c:out value="${requestScope.driverOffer.price}"/></strong><br/>
    <strong><fmt:message key="offer.description"/> <c:out value="${requestScope.driverOffer.offerDescription}"/></strong><br/>
</div>
