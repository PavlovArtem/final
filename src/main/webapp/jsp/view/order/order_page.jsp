<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/21/2020
  Time: 9:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.application.SecurityContext" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>


<div class="container is-centered">
    <c:choose>
        <c:when test="${not empty requestScope.orders_list}">
            <ul>
                <c:forEach var="orderDto" items="${requestScope.orders_list}">
                    <li>
                        <div class="box has-text-centered">
                            <c:set var="orderDto" value="${orderDto}" scope="request"/>
                            <jsp:include page="order_card.jsp"/>

                            <c:if test="${orderDto.orderStatus ne 'FINISHED' && orderDto.orderStatus ne 'IN_PROGRESS'}">
                                <c:if test="${SecurityContext.getInstance().canExecute(CommandType.START_ORDER_EXECUTION_COMMAND)}">
                                    <form action="${pageContext.request.contextPath}/" method="post">
                                        <input type="hidden" name="order_id" value="${orderDto.id}">
                                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                                               value="${CommandType.START_ORDER_EXECUTION_COMMAND}">
                                        <button class="button is-success"><fmt:message key="order.start"/></button>
                                    </form>
                                </c:if>
                                <c:if test="${SecurityContext.getInstance().canExecute(CommandType.CANCEL_ORDER_COMMAND)}">
                                    <form action="${pageContext.request.contextPath}/" method="post">
                                        <input type="hidden" name="order_id" value="${orderDto.id}">
                                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                                               value="${CommandType.CANCEL_ORDER_COMMAND}">
                                        <button class="button is-danger"><fmt:message key="order.cancel"/></button>
                                    </form>
                                </c:if>
                            </c:if>
                            <c:if test="${orderDto.orderStatus eq 'IN_PROGRESS'}">
                                <c:if test="${SecurityContext.getInstance().canExecute(CommandType.FINISH_ORDER_COMMAND)}">
                                    <form action="${pageContext.request.contextPath}/" method="post">
                                        <input type="hidden" name="order_id" value="${orderDto.id}">
                                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                                               value="${CommandType.FINISH_ORDER_COMMAND}">
                                        <button class="button is-success"><fmt:message key="order.finish"/></button>
                                    </form>
                                </c:if>
                            </c:if>
                        </div>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <div class="container is-centered ">
                <form class="box has-text-centered" action="" method="get">
                    <p><fmt:message key="order.not.found"/></p>
                    <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                           value="${CommandType.CREATE_FREIGHT_REQUEST}">
                    <button class="button is-primary"><fmt:message key="links.client.create.request"/></button>
                </form>
            </div>
        </c:otherwise>
    </c:choose>
</div>
