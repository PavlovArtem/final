<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/19/2020
  Time: 1:14 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<div class="container is-centered">
<c:choose>
    <c:when test="${not empty requestScope.freight_request_list}">

        <strong><fmt:message key="request.offers.requests"/></strong>
        <ul>
            <c:forEach var="freight_dto" items="${requestScope.freight_request_list}">
                <li class="box is-centered has-text-centered">
                    <c:set var="freight_dto" value="${freight_dto}" scope="request"/>
                    <jsp:include page="freight_card.jsp"/>
                    <form action="${pageContext.request.contextPath}/" method="get">
                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                               value="${CommandType.SHOW_OFFERS_COMMAND}">
                        <input type="hidden" id="freight_id" name="freight_id" value="${freight_dto.id}">

                        <button class="button is-primary">
                            <fmt:message key="request.show.offers"/></button>
                    </form>
                </li>
            </c:forEach>
        </ul>
    </c:when>
    <c:otherwise>
        <div class="notification is-centered has-text-centered">
            <strong> Open requests not found</strong>
        </div>
    </c:otherwise>
</c:choose>


<script>

    function addOfferToTable() {

    }

    function addFreightId(id) {
        let idInput = document.getElementById("freight_id");
        idInput.setAttribute('value', id);
    }
</script>
</div>
