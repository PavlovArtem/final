<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/15/2020
  Time: 8:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
    .map {
        height: 300px;
        width: 300px;
        position : relative;
        z-index: 2;
    }
</style>
<div class="container">
<%--    <div id="map" class="map">--%>
<%--        <script>--%>

<%--            var rootContext = "${pageContext.request.contextPath}static/leaflet/start.png";--%>
<%--            var map = L.map('map').setView([53.893009, 27.567444], 13);--%>
<%--            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {--%>
<%--                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',--%>
<%--                maxZoom: 18,--%>
<%--                id: 'mapbox/streets-v11',--%>
<%--                tileSize: 512,--%>
<%--                zoomOffset: -1,--%>
<%--                accessToken: 'pk.eyJ1IjoibXlzdGljYWxoIiwiYSI6ImNrY2t2MGowajF6Zzkyc254OTc0Y25rMTkifQ.NsS_HWrcJ1zg969wKEaROA'--%>
<%--            }).addTo(map);--%>

<%--            function setMarkers() {--%>

<%--            }--%>

<%--        </script>--%>
<%--    </div>--%>
    <b><fmt:message key="freight.request.cargo.weight"/><c:out value="${requestScope.freight_dto.cargoWeight}"/></b><br/>
    <b><fmt:message key="freight.request.cargo.volume"/><c:out value="${requestScope.freight_dto.cargoVolume}"/></b><br/>
    <b><fmt:message key="freight.request.cargo.datetime"/> <fmt:formatDate
            value="${requestScope.freight_dto.freightDate.time}" type="both" dateStyle="short"
            timeStyle="medium"/> </b><br/>

</div>




