<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.application.SecurityContext" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<div class="container is-centered">
    <c:choose>
        <c:when test="${SecurityContext.getInstance().isLoggedIn() ne true}">
            <div class="notification is-centered has-text-centered">
                <strong><fmt:message key="general.greetings.logout"/> </strong>
                <div class="is-centered">
                    <form action="" method="get">

                        <button class="button is-primary" name="${ApplicationConstants.COMMAND_PARAM}"
                                value="${CommandType.SIGN_UP_DISPLAY_COMMAND}" ><fmt:message key="links.user.sign.up"/></button>
                        <button class="button is-primary" name="${ApplicationConstants.COMMAND_PARAM}"
                                value="${CommandType.LOGIN_DISPLAY_COMMAND}" ><fmt:message key="links.user.login"/></button>
                    </form>
                </div>
            </div>
        </c:when>
    </c:choose>


</div>
