<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/15/2020
  Time: 6:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.application.SecurityContext" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>

<div class="container">
    <div class="column is-centered">
        <div class="content">
            <strong> <fmt:message key="request.offers.requests"/></strong>
            <div class="box">
                <jsp:include page="request/freight_card.jsp"/>
                <c:if test="${SecurityContext.getInstance().canExecute(CommandType.REQUEST_CANCEL_COMMAND)}">
                    <form action="${pageContext.request.contextPath}/" method="POST">
                        <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                               value="${CommandType.REQUEST_CANCEL_COMMAND}">
                        <input type="hidden" name="freight_id" value="${requestScope.freight_dto.id}">
                        <button type="submit" class="button is-danger"><fmt:message key="request.cancel"/></button>
                    </form>
                </c:if>
            </div>
        </div>
        <div class="container">
            <strong><fmt:message key="request.offers.offers"/></strong>
            <div class="container">
                <c:choose>
                    <c:when test="${not empty requestScope.display_offer_dto_list}">

                        <ul>
                            <c:forEach var="display_offer_dto" items="${requestScope.display_offer_dto_list}">
                                <li>
                                    <c:set var="display_offer_dto" value="${display_offer_dto}" scope="request"/>
                                    <div class="box">
                                        <jsp:include page="../view/offer/offer_card.jsp"/>
                                        <form  action="${pageContext.request.contextPath}/" method="POST" id="accept_form">
                                            <input type="hidden" name="${ApplicationConstants.COMMAND_PARAM}"
                                                   value="${CommandType.ACCEPT_OFFER_COMMAND}">
                                            <input type="hidden" name="offer_id" value="${display_offer_dto.offerId}" form="accept_form">
                                            <button  type="submit" class="button is-primary" form="accept_form"  ><fmt:message key="request.accept"/> </button>
                                        </form>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:when>
                    <c:otherwise>
                        <div class="box is-centered has-text-centered">
                            <p><fmt:message key="offer.no.offers.yet"/></p>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>

        </div>
    </div>
</div>


