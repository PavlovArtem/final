<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/25/2020
  Time: 3:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>

<div class="container">
    <div class="table is-striped">
        <table class="table is-striped">
            <tr>
                <th><fmt:message key="user.id"/> </th>
                <th><fmt:message key="user.email"/> </th>
                <th><fmt:message key="user.firstName"/> </th>
                <th><fmt:message key="user.lastName"/> </th>
                <th><fmt:message key="user.phone"/> </th>
                <th><fmt:message key="user.role"/> </th>
                <th><fmt:message key="user.is.blocked"/> </th>
            </tr>
            <c:forEach var="user" items="${requestScope.user_accounts}">
                <tr>
                    <td>
                        <c:out value="${user.id}"/>
                    </td>
                    <td>
                        <c:out value="${user.email}"/>
                    </td>
                    <td>
                        <c:out value="${user.name}"/>
                    </td>
                    <td>
                        <c:out value="${user.lastName}"/>
                    </td>
                    <td>
                        <c:out value="${user.phone}"/>
                    </td>
                    <td>
                        <div class="dropdown is-hoverable">
                            <ul>
                            <c:forEach var="role" items="${user.userRoles}">
                                <li>
                                    <div class="dropdown-item">
                                        <c:out value="${role.name()}"/>
                                    </div>
                                </li>
                            </c:forEach>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <c:out value="${user.active}"/>
                    </td>
                </tr>
            </c:forEach>


        </table>

    </div>
</div>

