<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/9/2020
  Time: 3:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.pavlov.fos.application.ApplicationConstants" %>
<%@ page import="by.pavlov.fos.application.SecurityContext" %>
<%@ page import="by.pavlov.fos.command.CommandType" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags" %>
<%--<nav class="navbar is-primary">--%>

<div class="navbar-end">
    <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
            Menu
        </a>
        <div class="navbar-dropdown is-right ">
            <ul>
                <c:if test="${SecurityContext.getInstance().isLoggedIn()}">
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.DISPLAY_ADMIN_DASHBOARD_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_ADMIN_DASHBOARD_COMMAND}">
                                <fmt:message key="links.user.list"/>
                            </a>
                        </li>
                    </c:if>

                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.CREATE_FREIGHT_REQUEST)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.CREATE_FREIGHT_REQUEST}">
                                <fmt:message key="links.client.create.request"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.SHOW_CLIENT_OPEN_REQUESTS_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.SHOW_CLIENT_OPEN_REQUESTS_COMMAND}">
                                <fmt:message key="links.client.show.client.open.requests"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.DISPLAY_WALLET_PAGE_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_WALLET_PAGE_COMMAND}">
                                <fmt:message key="user.wallet.info"/>
                            </a>
                        </li>
                    </c:if>
<%--                    driver actions--%>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.DISPLAY_DRIVER_PAGE_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_DRIVER_PAGE_COMMAND}">
                                <fmt:message key="offer.show.open.request"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.TRANSPORT_DISPLAY_CREATION_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.TRANSPORT_DISPLAY_CREATION_COMMAND}">
                                <fmt:message key="transport.create"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.SHOW_TRANSPORT_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.SHOW_TRANSPORT_COMMAND}">
                                <fmt:message key="transport.show"/>
                            </a>
                        </li>
                    </c:if>
<%--                    orders--%>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND}">
                                <fmt:message key="order.unfinished.orders"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${SecurityContext.getInstance().canExecute(CommandType.DISPLAY_FINISHED_ORDERS)}">
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_FINISHED_ORDERS}">
                                <fmt:message key="order.finished.orders"/>
                            </a>
                        </li>
                    </c:if>
                </c:if>

                <c:choose>
                    <c:when test="${SecurityContext.getInstance().isLoggedIn()}">
                        <li class="navbar-item active">
                            <a
                                    href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.LOG_OUT_COMMAND}">
                                <fmt:message key="links.user.logout"/>
                            </a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.LOGIN_DISPLAY_COMMAND}">
                                <fmt:message key="links.user.login"/>
                            </a>
                        </li>
                        <li class="navbar-item active">
                            <a class="navbar-item"
                               href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.SIGN_UP_DISPLAY_COMMAND}">
                                <fmt:message key="links.user.sign.up"/>
                            </a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>

    </div>
</div>
<%--</nav>--%>

