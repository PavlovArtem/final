<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/6/2020
  Time: 7:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="/jsp/error.jsp" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags/error" %>
<%@ taglib prefix="success" tagdir="/WEB-INF/tags/success" %>
<%@ taglib prefix="fmr" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Freight ordering system</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bulma.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/js/leaflet/leaflet.css">
    <script src="<c:url value='/static/js/leaflet/leaflet.js'/>"></script>
    <link rel="stylesheet" href="<c:url value='/static/js/routing_machine/leaflet-routing-machine.css'/>">
    <script src="<c:url value='/static/js/routing_machine/leaflet-routing-machine.js'/>"></script>
    <script src="<c:url value='/static/js/jquery-3.5.1.min.js'/>"></script>

    <c:choose>
        <c:when test="${('en').equals(requestScope.lang)}">
            <fmt:setLocale value="en"/>
        </c:when>
        <c:otherwise>
            <fmt:setLocale value="ru"/>
        </c:otherwise>
    </c:choose>

    <fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>


    <style>

        .hero-body.has-background {
            position: relative;
            overflow: hidden;
        }
        .hero-background {
            background-image: url("<c:url value='/static/css/image/back.jpg'/>");
            position: fixed;
            object-fit: cover;
            object-position: center center;
            width: 100%;
            height: 100%;
        }
        .hero-background.is-transparent {
            opacity: 0.3;
        }

        .box{
            opacity: 0.7;
        }

    </style>
</head>
<%--<body class="layout-documentation page-layout">--%>

<body  class="hero is-fullheight has-background">

<img alt="Fill Murray" class="hero-background is-transparent" src=" <c:url value='/static/css/image/back.jpg'/>" />
    <div class="hero-head">
        <header class="navbar is-primary is-fixed-top-desktop">

            <div class="navbar-start is-left">
                <lang:lang/>
            </div>
            <div class="navbar-item is-centered">
                <h1><strong><fmt:message key="app.title"/> </strong></h1>
            </div>
            <jsp:include page="nav_bar.jsp"/>
        </header>
    </div>

    <div class="hero-body has-background ">
        <div class="column">
            <success:success/>
            <error:error/>
            <div class="column">
                <jsp:include page="view/${viewName}.jsp"/>
            </div>
        </div>

    </div>
    <div class="hero-foot has-background-primary is-fixed-bottom is-transparent">
        <div class="content has-text-centered">
            <p>
            <h6><fmr:message key="app.subtitle"/></h6>
            </p>
        </div>
    </div>
<%--</div>--%>

</body>
</html>
