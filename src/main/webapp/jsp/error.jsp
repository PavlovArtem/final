<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 7/6/2020
  Time: 7:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags/error" %>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html>

<head>
    <title>ERROR</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bulma.css/bulma.css">
</head>

<body>
<section class="hero is-danger">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                <fmt:message key="app.title"/>
            </h1>
            <h2 class="subtitle">
                <fmt:message key="error.message"/>
            </h2>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <h1 class="title">SORRY AN EXCEPTION OCCURED</h1>
        <h2 class="subtitle">
            <error:error/>
            Exception is: <%= exception %>
        </h2>
    </div>
</section>
</body>

</html>
