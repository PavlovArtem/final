package by.pavlov.fos.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class DriverOffer {

    private Long id;
    private BigDecimal price;
    private String offerDescription;
    private OfferStatus offerStatus;
    private Long transportId;
    private Long freightRequestId;
    private Long userAccountId;


    public DriverOffer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public OfferStatus getOfferStatus() {
        return offerStatus;
    }

    public void setOfferStatus(OfferStatus offerStatus) {
        this.offerStatus = offerStatus;
    }

    public Long getTransportId() {
        return transportId;
    }

    public void setTransportId(Long transportId) {
        this.transportId = transportId;
    }

    public Long getFreightRequestId() {
        return freightRequestId;
    }

    public void setFreightRequestId(Long freightRequestId) {
        this.freightRequestId = freightRequestId;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DriverOffer)) return false;
        DriverOffer that = (DriverOffer) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(price, that.price) &&
                Objects.equals(offerDescription, that.offerDescription) &&
                offerStatus == that.offerStatus &&
                Objects.equals(transportId, that.transportId) &&
                Objects.equals(freightRequestId, that.freightRequestId) &&
                Objects.equals(userAccountId, that.userAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, offerDescription, offerStatus, transportId, freightRequestId, userAccountId);
    }

    @Override
    public String toString() {
        return "DriverOffer{" +
                "id=" + id +
                ", price=" + price +
                ", offerDescription='" + offerDescription + '\'' +
                ", offerStatus=" + offerStatus +
                ", transportId=" + transportId +
                ", freightRequestId=" + freightRequestId +
                ", userAccountId=" + userAccountId +
                '}';
    }

    public enum OfferStatus{

        WAITING,
        ACCEPT,
        DECLINE,
        CANCELED;

    }
}
