package by.pavlov.fos.entity;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

public class FreightRequest {

    private Long id;
    private Double cargoWeight;
    private Double cargoVolume;
    private Calendar freightDate;
    private RequestStatus requestStatus;
    private String description;
    private byte[] photo;
    private Long userAccountId;
    private Long routeId;

    public FreightRequest(Long id, Double cargoWeight, Double cargoVolume, Calendar freightDate, String description, byte[] photo, Long userAccountId, Long routeId) {
        this.id = id;
        this.cargoWeight = cargoWeight;
        this.cargoVolume = cargoVolume;
        this.freightDate = freightDate;
        this.description = description;
        this.photo = photo;
        this.userAccountId = userAccountId;
        this.routeId = routeId;
    }

    public FreightRequest() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(Double cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public Double getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(Double cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    public Calendar getFreightDate() {
        return freightDate;
    }

    public void setFreightDate(Calendar freightDate) {
        this.freightDate = freightDate;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FreightRequest that = (FreightRequest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(cargoWeight, that.cargoWeight) &&
                Objects.equals(cargoVolume, that.cargoVolume) &&
                Objects.equals(freightDate, that.freightDate) &&
                Objects.equals(description, that.description) &&
                Arrays.equals(photo, that.photo) &&
                Objects.equals(userAccountId, that.userAccountId) &&
                Objects.equals(routeId, that.routeId);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, cargoWeight, cargoVolume, freightDate, description, userAccountId, routeId);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }


    @Override
    public String toString() {
        return "FreightRequest{" +
                "id=" + id +
                ", cargoWeight=" + cargoWeight +
                ", cargoVolume=" + cargoVolume +
                ", orderDateTime=" + freightDate +
                ", description='" + description + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", userAccountId=" + userAccountId +
                ", routeId=" + routeId +
                '}';
    }


    public enum RequestStatus{

        OPEN,
        CLOSE,
        CANCELED;

    }
}
