package by.pavlov.fos.entity;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.validation.IsEmailValid;
import by.pavlov.fos.validation.MinLength;
import by.pavlov.fos.validation.ValidEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@ValidEntity("userAccount")
public class UserAccount {

    private Long id;
    @MinLength(2)
    private String name;
    private String lastName;
    @IsEmailValid(regex = ApplicationConstants.EMAIL_REGEX_PATTERN)
    private String email;
    @MinLength(3)
    private String password;
    private String phone;
    private boolean isActive;
    private Long walletId;
    private List<UserRole> userRoles = new ArrayList<>();

    public UserAccount(Long id, String name, String lastName, String email, String password, String phone, boolean isActive, Long walletId) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.isActive = isActive;
        this.walletId = walletId;
    }

    public UserAccount() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return isActive == that.isActive &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(email, that.email) &&
                Objects.equals(password, that.password) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(walletId, that.walletId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, email, password, phone, isActive, walletId);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", isActive=" + isActive +
                ", walletId=" + walletId +
                '}';
    }
}
