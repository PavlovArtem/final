package by.pavlov.fos.entity;

public enum UserRole {

    SUPER_ADMIN(1L),
    ADMIN(2L),
    DRIVER(3L),
    CLIENT(4L);

    private Long id;

    UserRole(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }


}
