package by.pavlov.fos.entity;

import java.util.Objects;

public class RoutePoint {

    private Long id;
    private String coordinates;
    private Long routeId;

    public RoutePoint(Long id, String coordinates, Long routeId) {
        this.id = id;
        this.coordinates = coordinates;
        this.routeId = routeId;
    }

    public RoutePoint() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoutePoint that = (RoutePoint) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(coordinates, that.coordinates) &&
                Objects.equals(routeId, that.routeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, coordinates, routeId);
    }

    @Override
    public String toString() {
        return "RoutePoint{" +
                "id=" + id +
                ", coordinates='" + coordinates + '\'' +
                ", routeId=" + routeId +
                '}';
    }
}
