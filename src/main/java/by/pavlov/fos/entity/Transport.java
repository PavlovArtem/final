package by.pavlov.fos.entity;

import java.util.Arrays;

public class Transport {

    private Long id;
    private String name;
    private Double maxWeight;
    private Double maxVolume;
    private String description;
    private byte[] photo;
    private Long userAccountId;


    public Transport() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Double getMaxVolume() {
        return maxVolume;
    }

    public void setMaxVolume(Double maxVolume) {
        this.maxVolume = maxVolume;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transport)) return false;

        Transport transport = (Transport) o;

        if (!id.equals(transport.id)) return false;
        if (name != null ? !name.equals(transport.name) : transport.name != null) return false;
        if (maxWeight != null ? !maxWeight.equals(transport.maxWeight) : transport.maxWeight != null) return false;
        if (maxVolume != null ? !maxVolume.equals(transport.maxVolume) : transport.maxVolume != null) return false;
        if (description != null ? !description.equals(transport.description) : transport.description != null)
            return false;
        if (!Arrays.equals(photo, transport.photo)) return false;
        return userAccountId != null ? userAccountId.equals(transport.userAccountId) : transport.userAccountId == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (maxWeight != null ? maxWeight.hashCode() : 0);
        result = 31 * result + (maxVolume != null ? maxVolume.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(photo);
        result = 31 * result + (userAccountId != null ? userAccountId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Transport{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maxWeight=" + maxWeight +
                ", maxVolume=" + maxVolume +
                ", description='" + description + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", userAccountId=" + userAccountId +
                '}';
    }
}
