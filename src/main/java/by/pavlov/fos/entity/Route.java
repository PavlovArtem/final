package by.pavlov.fos.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Route {

    private Long id;
    private String description;
    private List<RoutePoint> routePoints = new ArrayList<>();

    public Route(Long id, String description, List<RoutePoint> routePoints) {
        this.id = id;
        this.description = description;
        this.routePoints = routePoints;
    }

    public Route() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RoutePoint> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<RoutePoint> routePoints) {
        this.routePoints = routePoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(id, route.id) &&
                Objects.equals(description, route.description) &&
                Objects.equals(routePoints, route.routePoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, routePoints);
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", routePoints=" + routePoints +
                '}';
    }
}
