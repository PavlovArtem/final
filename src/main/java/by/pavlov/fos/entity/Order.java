package by.pavlov.fos.entity;

import java.util.Objects;

public class Order {

    private Long id;
    private OrderStatus orderStatus;
    private Long freightId;
    private Long driverOfferId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getFreightId() {
        return freightId;
    }

    public void setFreightId(Long freightId) {
        this.freightId = freightId;
    }

    public Long getDriverOfferId() {
        return driverOfferId;
    }

    public void setDriverOfferId(Long driverOfferId) {
        this.driverOfferId = driverOfferId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                orderStatus == order.orderStatus &&
                Objects.equals(freightId, order.freightId) &&
                Objects.equals(driverOfferId, order.driverOfferId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderStatus, freightId, driverOfferId);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderStatus=" + orderStatus +
                ", freight_id=" + freightId +
                ", order_id=" + driverOfferId +
                '}';
    }

    public enum OrderStatus {

        CANCELED,
        NOT_STARTED,
        IN_PROGRESS,
        FINISHED;

    }
}
