package by.pavlov.fos.connection;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection();

    void shutdown();

}
