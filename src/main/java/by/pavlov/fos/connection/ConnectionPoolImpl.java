package by.pavlov.fos.connection;

import by.pavlov.fos.application.ApplicationConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class ConnectionPoolImpl implements ConnectionPool {


    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolImpl.class);

    private final String databaseProperties;
    private final String dbUrl;
    private final String user;
    private final String password;
    private final int poolCapacity;


    private final BlockingQueue<Connection> availableConnections;
    private final BlockingQueue<Connection> usedConnections;


    public ConnectionPoolImpl() {

        this.databaseProperties = ApplicationConstants.DATABASE_PROPERTIES_PATH;
        Properties properties = readProperties(databaseProperties);
        String driver = properties.getProperty("JDBCDRIVER_CLASS");
        this.dbUrl = properties.getProperty("DB_URL");
        this.user = properties.getProperty("DB_USER");
        this.password = properties.getProperty("DB_PASSWORD");
        this.poolCapacity = Integer.parseInt(properties.getProperty("POOL_CAPACITY"));

        initDriver(driver);

        this.availableConnections = new LinkedBlockingQueue<>(this.poolCapacity);
        this.usedConnections = new LinkedBlockingQueue<>(this.poolCapacity);

        fillPool();

    }

    private Properties readProperties(String path) {

        Properties properties = new Properties();
        try (InputStream inputStream = ConnectionPoolImpl.class.getClassLoader().getResourceAsStream(path)) {
            assert inputStream != null;
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Failed to read database properties", e);
            throw new IllegalStateException(e.getMessage(), e);
        }
        return properties;
    }

    private void initDriver(String driverClass) {

        try {
            Class.forName(driverClass);
            LOGGER.info("Connection to database successful");
        } catch (ClassNotFoundException e) {
            LOGGER.error("driver not found", e);
            throw new IllegalStateException(e);
        }
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    void releaseConnection(Connection connection) {
        try {
            if (availableConnections.size() >= poolCapacity) {
                throw new IllegalStateException("Maximum pool size was reached");
            }

            usedConnections.remove(connection);
            availableConnections.put(connection);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    @Override
    public Connection getConnection() {
        Connection proxyConnection = null;
        try {
            Connection connection = availableConnections.take();
            usedConnections.put(connection);
            proxyConnection = createProxyConnection(connection);
        } catch (InterruptedException e) {
            LOGGER.error("Error while in connection pool", e);
            throw new ConnectionException(e.getMessage(), e);
        }

        return proxyConnection;
    }

    private void fillPool() {
        for (int i = 0; i < poolCapacity; i++) {
            Connection connection;
            try {
                connection = DriverManager.getConnection(dbUrl, user, password);
                availableConnections.put(connection);
            } catch (SQLException | InterruptedException e) {
                LOGGER.error("error while filling connection pool");
                throw new ConnectionException(e.getMessage(), e);
            }

        }
    }

    @Override
    public void shutdown() {

        availableConnections.clear();
        usedConnections.clear();
    }
}
