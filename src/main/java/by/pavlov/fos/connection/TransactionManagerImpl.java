package by.pavlov.fos.connection;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;


public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = LogManager.getLogger(TransactionManagerImpl.class);

    private final ConnectionPool connectionPool;
    private final ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionManagerImpl(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }


    @Override
    public void beginTransaction() throws SQLException {
        if (isEmpty()) {
            try {
                Connection connection = connectionPool.getConnection();
                connection.setAutoCommit(false);
                localConnection.set(connection);
            } catch (Exception e) {
                throw new SQLException(e);
            }

        } else {
            LOGGER.warn("Transaction already started");
        }
    }

    @Override
    public void commitTransaction() throws SQLException {
        try {
            Connection connection = localConnection.get();
            if (connection != null) {
                connection.commit();
                connection.setAutoCommit(true);
                connection.close();
            }
            localConnection.remove();
        } catch (Exception e) {
            throw new SQLException(e);
        }
    }

    @Override
    public void rollbackTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.rollback();
            connection.setAutoCommit(true);
            connection.close();
        }
        localConnection.remove();
    }


    @Override
    public Connection getConnection() throws SQLException {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if ("close".equals(method.getName())) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            return connectionPool.getConnection();
        }

    }

    @Override
    public boolean isEmpty() {
        Connection connection = localConnection.get();
        return connection == null;
    }

}
