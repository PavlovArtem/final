package by.pavlov.fos.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;


public class ConnectionManagerImpl implements ConnectionManager {


    private static final Logger LOGGER = LogManager.getLogger(ConnectionManagerImpl.class);

    private final ConnectionPool connectionPool;
    private final TransactionManager transactionManager;


    public ConnectionManagerImpl(ConnectionPool connectionPool, TransactionManager transactionManager) {
        this.connectionPool = connectionPool;
        this.transactionManager = transactionManager;
    }


    @Override
    public Connection getConnection() throws SQLException {

        Connection connection = transactionManager.getConnection();
        return connection != null ? connection : connectionPool.getConnection();
    }

    @Override
    public void shutdown() {
        connectionPool.shutdown();
    }


}
