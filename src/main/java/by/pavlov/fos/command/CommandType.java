package by.pavlov.fos.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {

    DISPLAY_ADMIN_DASHBOARD_COMMAND,
    //Common commands
    LOGIN_DISPLAY_COMMAND,
    LOGIN_COMMAND,
    SIGN_UP_DISPLAY_COMMAND,
    SIGN_UP_COMMAND,
    LOG_OUT_COMMAND,
    UPDATE_USER_COMMAND,
    DISPLAY_WALLET_PAGE_COMMAND,
    FILL_UP_WALLET_COMMAND,
    INDEX_COMMAND,
    CREATE_FREIGHT_REQUEST,
    SHOW_OPEN_REQUESTS,
    CREATE_DRIVER_OFFER_COMMAND,
    TRANSPORT_DISPLAY_CREATION_COMMAND,
    DISPLAY_DRIVER_PAGE_COMMAND,
    CREATE_TRANSPORT_COMMAND,
    TRANSPORT_DISPLAY_UPDATE_COMMAND,
    TRANSPORT_UPDATE_COMMAND,
    TRANSPORT_DELETE_COMMAND,
    REQUEST_DETAILS_COMMAND,
    REQUEST_CANCEL_COMMAND,
    SHOW_CLIENT_OPEN_REQUESTS_COMMAND,
    SHOW_TRANSPORT_COMMAND,
    SHOW_OFFERS_COMMAND,
    ACCEPT_OFFER_COMMAND,
    DISPLAY_UNFINISHED_ORDERS_COMMAND,
    DISPLAY_FINISHED_ORDERS,
    OFFER_GET_COMMAND,
    CANCEL_ORDER_COMMAND,
    START_ORDER_EXECUTION_COMMAND,
    FINISH_ORDER_COMMAND,
    ERROR_COMMAND;

    public static Optional<CommandType> of(String name) {
        return Stream.of(CommandType.values())
                .filter(commandType -> commandType.name()
                        .equalsIgnoreCase(name))
                .findFirst();
    }

}
