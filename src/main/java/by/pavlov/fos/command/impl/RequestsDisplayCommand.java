package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.service.ClientFreightService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RequestsDisplayCommand extends AbstractCommand {

    private final ClientFreightService clientFreightService;

    public RequestsDisplayCommand(ClientFreightService clientFreightService) {
        this.clientFreightService = clientFreightService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Long userId = SecurityContext.getInstance().getCurrentUser().getId();
        List<FreightRequestDto> freightRequestDtoList = clientFreightService.getAllOpenUserRequests(userId);
        request.setAttribute("freight_request_list", freightRequestDtoList);
        forward(request, response, "/request/open_request_list");
    }
}
