package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.TransportDto;
import by.pavlov.fos.service.TransportService;
import by.pavlov.fos.validation.EntityValidator;
import by.pavlov.fos.validation.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class TransportCreateCommand extends AbstractCommand {

    private final TransportService transportService;
    private final EntityValidator entityValidator;

    public TransportCreateCommand(TransportService transportService, EntityValidator entityValidator) {
        this.transportService = transportService;
        this.entityValidator = entityValidator;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {


        TransportDto transportDto = new TransportDto();
        transportDto.setName(request.getParameter(ApplicationConstants.TRANSPORT_NAME));
        transportDto.setMaxWeight(Double.valueOf(request.getParameter(ApplicationConstants.TRANSPORT_MAX_WEIGHT)));
        transportDto.setMaxVolume(Double.valueOf(request.getParameter(ApplicationConstants.TRANSPORT_MAX_VOLUME)));
        transportDto.setDescription(request.getParameter(ApplicationConstants.TRANSPORT_DESCRIPTION));
        InputStream inputStream = request.getPart(ApplicationConstants.TRANSPORT_IMAGE).getInputStream();
        transportDto.setPhoto(getPhoto(inputStream));
        transportDto.setUserAccountId(SecurityContext.getInstance().getCurrentUser().getId());

        ValidationResult validationResult = validate(transportDto);
        if (validationResult.getErrorMessages().isEmpty()) {
            transportService.createTransport(transportDto);
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.SHOW_TRANSPORT_COMMAND);
        } else {
            request.setAttribute("errorMessages", validationResult.getErrorMessages());
            forward(request, response, ApplicationConstants.CREATE_TRANSPORT_PAGE);
        }
    }


    private ValidationResult validate(TransportDto transportDto) {

        return entityValidator.validate(transportDto);

    }


    private byte[] getPhoto(InputStream inputStream) throws IOException {

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        return buffer.toByteArray();
    }

}
