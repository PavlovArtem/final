package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.service.DriverFreightService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OfferGetCommand extends AbstractCommand {

    private final DriverFreightService driverFreightService;

    public OfferGetCommand(DriverFreightService driverFreightService) {
        this.driverFreightService = driverFreightService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long userId = SecurityContext.getInstance().getCurrentUser().getId();
        Long requestId = Long.parseLong(request.getParameter("request_id"));

        DisplayOfferDto offerDto = driverFreightService.getDriverOfferForRequest(userId, requestId);
        request.setAttribute("offer_dto", offerDto);
        forward(request, response, "/offer/offer_card");

    }
}
