package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.DriverOffer;
import by.pavlov.fos.service.ClientFreightService;
import by.pavlov.fos.service.DriverFreightService;
import by.pavlov.fos.service.OrderService;
import by.pavlov.fos.service.WalletService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class OfferAcceptCommand extends AbstractCommand {


    private final ClientFreightService clientFreightService;
    private final DriverFreightService driverFreightService;
    private final OrderService orderService;
    private final WalletService walletService;

    public OfferAcceptCommand(ClientFreightService clientFreightService, DriverFreightService driverFreightService, OrderService orderService, WalletService walletService) {
        this.clientFreightService = clientFreightService;
        this.driverFreightService = driverFreightService;
        this.orderService = orderService;
        this.walletService = walletService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        canExecute(request, response, CommandType.ACCEPT_OFFER_COMMAND);

        Long offerId = Long.parseLong(request.getParameter("offer_id"));


        DriverOffer driverOffer = clientFreightService.findOfferById(offerId);
        if (walletService.isEnoughMoney(driverOffer.getPrice(), SecurityContext.getInstance().getCurrentUser().getWalletId())) {
            orderService.confirmOfferAndCreateOrder(offerId);
            redirect(response, "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND +
                    "&isSuccess= offer was accepted");

        } else {
            List<String> errorMessages = new ArrayList<>();
            errorMessages.add("Not enough money on the count");
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("request_id", driverOffer.getFreightRequestId());
            forward(request, response, "freight_request");
        }


    }


}
