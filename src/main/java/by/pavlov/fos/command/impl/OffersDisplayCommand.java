package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.service.ClientFreightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class OffersDisplayCommand extends AbstractCommand {

    private static final Logger LOGGER = LogManager.getLogger(OffersDisplayCommand.class);

    private final ClientFreightService clientFreightService;

    public OffersDisplayCommand(ClientFreightService clientFreightService) {
        this.clientFreightService = clientFreightService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.SHOW_OFFERS_COMMAND);


            Long requestId = Long.valueOf(request.getParameter("freight_id"));
            FreightRequestDto freightRequestDto = clientFreightService.getFreightRequest(requestId);
            List<DisplayOfferDto> displayOfferDtoList = clientFreightService.getOffers(requestId);
            request.setAttribute("display_offer_dto_list", displayOfferDtoList);
            request.setAttribute(ApplicationConstants.FREIGHT_REQUEST_DTO, freightRequestDto);
            forward(request, response, "freight_request");

    }
}
