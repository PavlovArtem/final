package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

public class OrderStartExecutionCommand extends AbstractCommand {

    private final OrderService orderService;

    public OrderStartExecutionCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.START_ORDER_EXECUTION_COMMAND);

        Long orderId = Long.parseLong(request.getParameter("order_id"));
        if (orderService.beginFreight(orderId)) {
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "="
                    + CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND + "&isSuccess=order execution was started");
        } else {
            List<String> errorMessages = Collections.singletonList("Something went wrong");
            request.setAttribute("errorMessages", errorMessages);
            forward(request, response, "order_page");
        }


    }
}
