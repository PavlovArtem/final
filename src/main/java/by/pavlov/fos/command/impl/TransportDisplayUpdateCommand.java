package by.pavlov.fos.command.impl;

import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.ShowTransportDto;
import by.pavlov.fos.dto.TransportDto;
import by.pavlov.fos.service.TransportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TransportDisplayUpdateCommand extends AbstractCommand {

    private final TransportService transportService;

    public TransportDisplayUpdateCommand(TransportService transportService) {
        this.transportService = transportService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.TRANSPORT_DISPLAY_UPDATE_COMMAND);

        Long transportId = Long.parseLong(request.getParameter("transport_id"));
        ShowTransportDto transportDto = transportService.getTransport(transportId);
        request.setAttribute("transport", transportDto);
        forward(request, response, "/transport/update_transport");

    }
}
