package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.service.ClientFreightService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestCancelCommand extends AbstractCommand {

    private final ClientFreightService clientFreightService;

    public RequestCancelCommand(ClientFreightService clientFreightService) {
        this.clientFreightService = clientFreightService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.REQUEST_CANCEL_COMMAND);

        Long id = Long.parseLong(request.getParameter("freight_id"));
        clientFreightService.deleteAllOffersByRequestId(id);
        if (clientFreightService.deleteFreightRequest(id)){
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.INDEX_COMMAND+ "&isSuccess=request #: " + id + " was deleted");
        } else {
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.INDEX_COMMAND+ "&errorMessages=error while cancel request");
        }

    }
}
