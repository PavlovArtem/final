package by.pavlov.fos.command.impl;

import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TransportUpdateCommand extends AbstractCommand {
    

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.TRANSPORT_UPDATE_COMMAND);




    }
}
