package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.service.WalletService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Collections;

import static by.pavlov.fos.application.ApplicationConstants.COMMAND_PARAM;

public class DisplayWalletPageCommand extends AbstractCommand {

    private final WalletService walletService;

    public DisplayWalletPageCommand(WalletService walletService) {
        this.walletService = walletService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        canExecute(request, response, CommandType.DISPLAY_WALLET_PAGE_COMMAND);
        if ("get".equalsIgnoreCase(request.getMethod())) {

            UserAccount userAccount = SecurityContext.getInstance().getCurrentUser();
            request.setAttribute("wallet", walletService.getWallet(userAccount.getWalletId()));
            forward(request, response, "wallet_page");

        } else if ("post".equalsIgnoreCase(request.getMethod())) {

            Long walletId = SecurityContext.getInstance().getCurrentUser().getWalletId();
            BigDecimal incomingAmount = new BigDecimal(request.getParameter("amount"));
            if (incomingAmount.compareTo(new BigDecimal("0.1")) <= 0) {
                request.setAttribute("errorMessages", Collections.singletonList("Amount can't be less than 0.1"));
                forward(request, response, "wallet_page");
            } else {
                walletService.fillUpWallet(incomingAmount, walletId);
                redirect(response, request.getContextPath() + "?" + COMMAND_PARAM + "=" + CommandType.DISPLAY_WALLET_PAGE_COMMAND + "&isSuccess=true");
            }
        }
    }
}
