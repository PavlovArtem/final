package by.pavlov.fos.command.impl;

import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.service.TransportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

import static by.pavlov.fos.application.ApplicationConstants.COMMAND_PARAM;

public class TransportDeleteCommand extends AbstractCommand {

    private final TransportService transportService;

    public TransportDeleteCommand(TransportService transportService) {
        this.transportService = transportService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.TRANSPORT_DELETE_COMMAND);

        Long tId = Long.parseLong(request.getParameter("transport_id"));
        if (transportService.deleteTransport(tId)) {
            redirect(response, request.getContextPath() + "?" + COMMAND_PARAM + "=" + CommandType.SHOW_TRANSPORT_COMMAND + "&isSuccess=transport was deleted");
        } else {
            request.setAttribute("errorMessages", Collections.singletonList("Something went wrong while delete transport"));
            forward(request, response, "transport/transport_list");
        }

    }
}
