package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogOutCommand extends AbstractCommand {


    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        SecurityContext.getInstance().logout(request.getSession().getId());
        redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + " = " + CommandType.INDEX_COMMAND);
    }
}
