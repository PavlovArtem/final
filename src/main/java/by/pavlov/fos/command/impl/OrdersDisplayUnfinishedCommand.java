package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class OrdersDisplayUnfinishedCommand extends AbstractCommand {

    private final OrderService orderService;

    public OrdersDisplayUnfinishedCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        canExecute(request, response, CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND);
        List<OrderDto> ordersList = orderService.findAllUnfinishedOrders(SecurityContext.getInstance().getCurrentUser());
        request.setAttribute("orders_list", ordersList);
        forward(request, response, "/order/order_page");

    }
}
