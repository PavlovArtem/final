package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

public class OrderCancelCommand extends AbstractCommand {

    private final OrderService orderService;

    public OrderCancelCommand(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request,response, CommandType.CANCEL_ORDER_COMMAND);

        Long orderId = Long.parseLong(request.getParameter("order_id"));
        if (orderService.cancelOrder(orderId)) {
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "="
                    + CommandType.INDEX_COMMAND + "&isSuccess=order #: " + orderId +" canceled successful");
        } else {
            List<String> errorMessages = Collections.singletonList("Something went wrong");
            request.setAttribute("errorMessages", errorMessages);
            forward(request, response, "order_page");
        }

    }
}
