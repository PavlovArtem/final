package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import by.pavlov.fos.service.ServiceException;
import by.pavlov.fos.service.UserAccountService;
import by.pavlov.fos.validation.EntityValidator;
import by.pavlov.fos.validation.ValidationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class SignUpCommand extends AbstractCommand {

    private static final Logger LOGGER = LogManager.getLogger(SignUpCommand.class);

    private final UserAccountService userAccountService;
    private final EntityValidator entityValidator;

    public SignUpCommand(UserAccountService userAccountService, EntityValidator entityValidator) {
        this.userAccountService = userAccountService;
        this.entityValidator = entityValidator;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String email = request.getParameter("uemail");
        String name = request.getParameter("uname");
        String lastName = request.getParameter("ulastname");
        String phone = request.getParameter("uphone");
        String password = request.getParameter("upassword");
        String role = request.getParameter("urole");

        UserAccount userAccount = createUserAccount(email, name, lastName, phone, password, role);

        ValidationResult validationResult = validateUser(userAccount);

        if (validationResult.getErrorMessages().isEmpty()) {
            userAccountService.signUp(userAccount);
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.LOGIN_DISPLAY_COMMAND + "&isSuccess=account was created");
        } else {

            request.setAttribute("errorMessages", validationResult.getErrorMessages());
            forward(request, response, ApplicationConstants.SIGN_UP_PAGE);

        }
    }

    private ValidationResult validateUser(UserAccount userAccount) throws ServiceException {
        ValidationResult validationResult = entityValidator.validate(userAccount);
        String emailExist = checkIsUserExist(userAccount.getEmail());
        if (emailExist != null) {
            validationResult.addErrorMessage(emailExist);
        }

        return validationResult;
    }

    private String checkIsUserExist(String email) throws ServiceException {
        if (userAccountService.findByEmail(email).isPresent()) {
            return "email already exist!";
        }
        return null;
    }


    private UserAccount createUserAccount(String email, String name, String lastName, String phone, String password, String role) {

        UserAccount userAccount = new UserAccount();
        userAccount.setEmail(email);
        userAccount.setName(name);
        userAccount.setLastName(lastName);
        userAccount.setPhone(phone);
        userAccount.setPassword(password);
        UserRole userRole = Arrays.stream(UserRole.values())
                .filter(e -> e.name().equalsIgnoreCase(role))
                .reduce((t1, t2) -> t1)
                .orElseThrow(() -> new IllegalStateException("Role not found"));
        userAccount.getUserRoles().add(userRole);
        return userAccount;
    }


}
