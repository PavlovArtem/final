package by.pavlov.fos.command.impl;

import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.service.UserAccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AdminDashboardCommand extends AbstractCommand {

    private final UserAccountService userAccountService;

    public AdminDashboardCommand(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }


    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.DISPLAY_ADMIN_DASHBOARD_COMMAND);

        List<UserAccount> userAccountList = userAccountService.findAll();
        request.setAttribute("user_accounts", userAccountList);
        forward(request, response, "admin/admin_dashboard");

    }
}
