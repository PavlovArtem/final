package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandException;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.service.ClientFreightService;
import by.pavlov.fos.validation.EntityValidator;
import by.pavlov.fos.validation.ValidationResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static by.pavlov.fos.application.ApplicationConstants.*;

public class RequestCreateCommand extends AbstractCommand {

    private static final Logger LOGGER = LogManager.getLogger(RequestCreateCommand.class);

    private final ClientFreightService clientFreightService;
    private final EntityValidator entityValidator;

    public RequestCreateCommand(ClientFreightService clientFreightService, EntityValidator entityValidator) {
        this.clientFreightService = clientFreightService;
        this.entityValidator = entityValidator;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if ("get".equalsIgnoreCase(request.getMethod())) {
            forward(request, response, ApplicationConstants.CREATE_REQUEST);
        } else {
            FreightRequestDto freightRequestDto = createFreightRequestDto(request);
            ValidationResult validationResult = validFreightDto(freightRequestDto);
            if (!validationResult.getErrorMessages().isEmpty()) {
                request.setAttribute("errorMessages", validationResult.getErrorMessages());
                forward(request, response, CREATE_REQUEST);
            } else {
                Long freightId = clientFreightService.createFreightRequest(freightRequestDto);
                redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.SHOW_OFFERS_COMMAND + "&freight_id=" + freightId + "&isSuccess=freight request created");
            }
        }
    }

    private ValidationResult validFreightDto(FreightRequestDto freightRequestDto) {

        ValidationResult validationResult = entityValidator.validate(freightRequestDto);

        if (freightRequestDto.getCoordinates().size() < 2) {
            validationResult.addErrorMessage("Must be at least 2 points");
        } else if (freightRequestDto.getFreightDate().before(Calendar.getInstance().getTime())) {
            validationResult.addErrorMessage("Date can't be less than current date");
        }

        return validationResult;
    }


    private FreightRequestDto createFreightRequestDto(HttpServletRequest request) throws CommandException {

        UserAccount currentUser = SecurityContext.getInstance().getCurrentUser();

        FreightRequestDto freightRequestDto = new FreightRequestDto();
        freightRequestDto.setUserId(currentUser.getId());

        freightRequestDto.setCargoWeight(Double.valueOf(request.getParameter(CARGO_WEIGHT)));
        freightRequestDto.setCargoVolume(Double.valueOf(request.getParameter(CARGO_VOLUME)));
        freightRequestDto.setFreightDate(parseDate(request.getParameter(FREIGHT_DATE)));
        freightRequestDto.setCoordinates(parseCoordinates(request.getParameter(FREIGHT_ROUTE_POINTS)));
        freightRequestDto.setDescription(request.getParameter(FREIGHT_DESCRIPTION));

        return freightRequestDto;
    }

    /**
     * @param inputCoordinates input example 53.896651435726824 : 27.55396842956543,53.9067652479072 : 27.591390609741214,53.896550285240444 : 27.6097583770752
     * @return list of strings where coordinates split like {53.896651435726824 , 27.55396842956543} ...
     */
    private List<String> parseCoordinates(String inputCoordinates) {

        String[] splitByPairs = inputCoordinates.split(",");
        List<String> preparedCoordinates = new ArrayList<>();
        for (String cor : splitByPairs) {
            preparedCoordinates.add(cor.replace(':', ','));
        }
        return preparedCoordinates;
    }


    /**
     * @param inputDate yyyy-MM-dd'T'HH:mm
     * @return calendar
     */
    private Calendar parseDate(String inputDate) throws CommandException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            calendar.setTime(format.parse(inputDate));
        } catch (ParseException e) {
            LOGGER.error("error while parsing date", e);
            throw new CommandException(e);
        }
        return calendar;
    }

}
