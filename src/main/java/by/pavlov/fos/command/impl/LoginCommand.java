package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.service.UserAccountService;
import by.pavlov.fos.validation.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class LoginCommand extends AbstractCommand {

    private final UserAccountService userAccountService;

    public LoginCommand(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {


        String email = request.getParameter("uemail");
        String password = request.getParameter("upassword");
        Optional<UserAccount> userAccount = Optional.ofNullable(userAccountService.login(email, password));

        ValidationResult validationResult = checkLogin(userAccount);

        if (validationResult.getErrorMessages().isEmpty()) {
            SecurityContext.getInstance().login(userAccount.get(), request.getSession().getId());
            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.INDEX_COMMAND);
        } else {
            request.setAttribute("errorMessages", validationResult.getErrorMessages());
            forward(request, response, ApplicationConstants.LOG_IN_PAGE);
        }


    }

    private ValidationResult checkLogin(Optional<UserAccount> userAccount) {
        ValidationResult validationResult = new ValidationResult();
        if (!userAccount.isPresent()) {
            validationResult.addErrorMessage("incorrect email or password");
        }
        return validationResult;
    }
}
