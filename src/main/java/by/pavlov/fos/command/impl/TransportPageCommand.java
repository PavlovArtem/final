package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.dto.ShowTransportDto;
import by.pavlov.fos.service.TransportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class TransportPageCommand extends AbstractCommand {

    private final TransportService transportService;

    public TransportPageCommand(TransportService transportService) {
        this.transportService = transportService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ShowTransportDto> showTransportDto = transportService.findAllUserTransport(SecurityContext.getInstance().getCurrentUser().getId());
        request.setAttribute("transport_dto_list", showTransportDto);
        forward(request, response, "/transport/transport_list");
    }
}
