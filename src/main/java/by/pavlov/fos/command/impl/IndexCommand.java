package by.pavlov.fos.command.impl;

import by.pavlov.fos.command.AbstractCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.pavlov.fos.application.ApplicationConstants.MAIN_PAGE;

public class IndexCommand extends AbstractCommand {

    private static final Logger LOGGER = LogManager.getLogger(IndexCommand.class);

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        forward(request, response, MAIN_PAGE);

    }
}
