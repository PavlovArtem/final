package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.service.WalletService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

import static by.pavlov.fos.application.ApplicationConstants.COMMAND_PARAM;

public class FillUpWalletCommand extends AbstractCommand {

    private final WalletService walletService;

    public FillUpWalletCommand(WalletService walletService) {
        this.walletService = walletService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        canExecute(request,response, CommandType.FILL_UP_WALLET_COMMAND);
        Long walletId = SecurityContext.getInstance().getCurrentUser().getWalletId();
        BigDecimal incomingAmount = new BigDecimal(request.getParameter("amount"));
        if (incomingAmount.compareTo(new BigDecimal("0.1")) <= 0 ){
            request.setAttribute("error", "Amount can't be less than 0.1");
            forward(request, response, "wallet_page");
        } else {
            walletService.fillUpWallet(incomingAmount, walletId);
            redirect(response, request.getContextPath() + "?" + COMMAND_PARAM + " = " + CommandType.DISPLAY_WALLET_PAGE_COMMAND + "&isSuccess=fill success");
        }

    }
}
