package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.service.DriverFreightService;
import by.pavlov.fos.service.TransportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayDriverPageCommand extends AbstractCommand {

    private final TransportService transportService;
    private final DriverFreightService driverFreightService;

    public DisplayDriverPageCommand(TransportService transportService, DriverFreightService driverFreightService) {
        this.transportService = transportService;
        this.driverFreightService = driverFreightService;
    }

    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Long userId = SecurityContext.getInstance().getCurrentUser().getId();

        request.setAttribute("transport_dto_list", transportService.findAllUserTransport(userId));
        request.setAttribute("freight_request_list", driverFreightService.getAllOpenRequests());
        forward(request, response, "/offer/driver_request_page");
    }
}
