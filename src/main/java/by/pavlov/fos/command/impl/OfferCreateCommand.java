package by.pavlov.fos.command.impl;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.dto.DriverOfferDto;
import by.pavlov.fos.service.DriverFreightService;
import by.pavlov.fos.service.TransportService;
import by.pavlov.fos.validation.EntityValidator;
import by.pavlov.fos.validation.ValidationResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

public class OfferCreateCommand extends AbstractCommand {

    private final DriverFreightService driverFreightService;
    private final TransportService transportService;
    private final EntityValidator entityValidator;

    public OfferCreateCommand(DriverFreightService driverFreightService, TransportService transportService, EntityValidator entityValidator) {
        this.driverFreightService = driverFreightService;
        this.transportService = transportService;
        this.entityValidator = entityValidator;
    }


    //TODO: add check of request!
    @Override
    protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
        canExecute(request, response, CommandType.CREATE_DRIVER_OFFER_COMMAND);
        Long userId = SecurityContext.getInstance().getCurrentUser().getId();

        try{

            DriverOfferDto driverOfferDto = new DriverOfferDto();
            driverOfferDto.setPrice(BigDecimal.valueOf(Double.parseDouble(request.getParameter("offer_price"))));
            String description = request.getParameter("offer_description");
            driverOfferDto.setOfferDescription(description != null ? description : "");
            driverOfferDto.setFreightRequestId(Long.valueOf(request.getParameter("freight_id")));
            driverOfferDto.setTransportId(Long.valueOf(request.getParameter("offer_transport_id")));
            driverOfferDto.setUserAccountId(userId);

            ValidationResult validationResult = validateOffer(driverOfferDto);
            if (validationResult.getErrorMessages().isEmpty()){
                driverFreightService.createOffer(driverOfferDto);
                redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.DISPLAY_DRIVER_PAGE_COMMAND +"&isSuccess=offer added successfully");
            } else {
                request.setAttribute("errorMessages", validationResult.getErrorMessages());
                forward(request,response, "driver_request_page");
            }


        } catch (Exception ex){


            redirect(response, request.getContextPath() + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.DISPLAY_DRIVER_PAGE_COMMAND +"&errorMessage=offer not added try again");

        }

    }


    private ValidationResult validateOffer(DriverOfferDto driverOfferDto){

        return entityValidator.validate(driverOfferDto);


    }


}
