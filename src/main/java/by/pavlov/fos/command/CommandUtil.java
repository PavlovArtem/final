package by.pavlov.fos.command;

import by.pavlov.fos.application.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;

public class CommandUtil {

    private CommandUtil(){

    }

    public static String getCommandFromRequest(HttpServletRequest request) {
        return request.getParameter(ApplicationConstants.COMMAND_PARAM) != null ?
                request.getParameter(ApplicationConstants.COMMAND_PARAM) :
                String.valueOf(request.getAttribute(ApplicationConstants.COMMAND_PARAM));

    }

}
