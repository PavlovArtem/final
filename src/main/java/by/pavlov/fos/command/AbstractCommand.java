package by.pavlov.fos.command;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.pavlov.fos.application.ApplicationConstants.COMMAND_PARAM;

public abstract class AbstractCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(AbstractCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            executeWrapped(request, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            forward(request, response, ApplicationConstants.ERROR_PAGE);
        }
    }

    protected abstract void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception;

    protected void forward(HttpServletRequest request, HttpServletResponse response, String viewName) {

        try {
            LOGGER.info("method: {} path: {}", request.getMethod(), request.getContextPath());

            request.setAttribute("viewName", viewName);
            request.getRequestDispatcher("jsp/main_layout.jsp").forward(request, response);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalStateException("Failed to forward request", e);
        }
    }

    protected void redirect(HttpServletResponse response, String redirect) {

        try {

            LOGGER.info(redirect);
            response.sendRedirect(redirect);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalStateException("Failed to redirect", e);
        }
    }

    protected void canExecute(HttpServletRequest request, HttpServletResponse response, CommandType commandType) {
        if (!SecurityContext.getInstance().canExecute(commandType)) {
            response.setStatus(403);
            redirect(response,  request.getContextPath() + "jsp/error.jsp?error=NOT ENOUGH PERMISSIONS");
        }
    }
}
