package by.pavlov.fos.command;

import java.util.EnumMap;
import java.util.Map;

public class BasicCommandProvider implements CommandProvider {

    private final Map<CommandType,Command> commandRegistry = new EnumMap<>(CommandType.class);

    @Override
    public void register(CommandType commandType, Command command) {
        this.commandRegistry.put(commandType, command);
    }

    @Override
    public void remove(CommandType commandType) {
        this.commandRegistry.remove(commandType);
    }

    @Override
    public Command getCommand(CommandType commandType) throws CommandException {

        if (this.commandRegistry.containsKey(commandType)){
            return this.commandRegistry.get(commandType);
        } else {
            throw new CommandException("Command not found");
        }
    }
}
