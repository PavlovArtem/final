package by.pavlov.fos.dto;



import by.pavlov.fos.entity.FreightRequest;
import by.pavlov.fos.validation.MinDoubleValue;
import by.pavlov.fos.validation.ValidEntity;

import java.util.*;


@ValidEntity("freightRequestDto")
public class FreightRequestDto {

    private Long id;
    private Long userId;
    @MinDoubleValue(0.1D)
    private Double cargoWeight;
    @MinDoubleValue(0.1D)
    private Double cargoVolume;
    private Calendar freightDate;
    private FreightRequest.RequestStatus requestStatus;
    private String description;
    private List<String> coordinates = new ArrayList<>();


    public FreightRequestDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Double getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(Double cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public Double getCargoVolume() {
        return cargoVolume;
    }

    public void setCargoVolume(Double cargoVolume) {
        this.cargoVolume = cargoVolume;
    }

    public Calendar getFreightDate() {
        return freightDate;
    }

    public void setFreightDate(Calendar freightDate) {
        this.freightDate = freightDate;
    }

    public FreightRequest.RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(FreightRequest.RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<String> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FreightRequestDto)) return false;

        FreightRequestDto that = (FreightRequestDto) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (cargoWeight != null ? !cargoWeight.equals(that.cargoWeight) : that.cargoWeight != null) return false;
        if (cargoVolume != null ? !cargoVolume.equals(that.cargoVolume) : that.cargoVolume != null) return false;
        if (freightDate != null ? !freightDate.equals(that.freightDate) : that.freightDate != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return coordinates != null ? coordinates.equals(that.coordinates) : that.coordinates == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (cargoWeight != null ? cargoWeight.hashCode() : 0);
        result = 31 * result + (cargoVolume != null ? cargoVolume.hashCode() : 0);
        result = 31 * result + (freightDate != null ? freightDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (coordinates != null ? coordinates.hashCode() : 0);
        return result;
    }

    @Override

    public String toString() {
        return "FreightRequestDto{" +
                "userId=" + userId +
                ", cargoWeight=" + cargoWeight +
                ", cargoVolume=" + cargoVolume +
                ", freightDate=" + freightDate +
                ", description='" + description + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
