package by.pavlov.fos.dto;

import by.pavlov.fos.entity.Order;

public class DisplayOrderDto {

    private Long orderId;
    private Order.OrderStatus orderStatus;

}
