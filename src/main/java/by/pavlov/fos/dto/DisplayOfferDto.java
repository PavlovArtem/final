package by.pavlov.fos.dto;

import java.math.BigDecimal;

public class DisplayOfferDto {

    private Long offerId;
    private BigDecimal price;
    private String transportEncodedPhoto;
    private String description;
    private Long freightId;


    public DisplayOfferDto() {
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTransportEncodedPhoto() {
        return transportEncodedPhoto;
    }

    public void setTransportEncodedPhoto(String transportEncodedPhoto) {
        this.transportEncodedPhoto = transportEncodedPhoto;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getFreightId() {
        return freightId;
    }

    public void setFreightId(Long freightId) {
        this.freightId = freightId;
    }
}
