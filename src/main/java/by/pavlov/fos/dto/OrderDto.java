package by.pavlov.fos.dto;

import by.pavlov.fos.entity.DriverOffer;
import by.pavlov.fos.entity.FreightRequest;
import by.pavlov.fos.entity.Order;

public class OrderDto {

    private Long id;
    private Order.OrderStatus orderStatus;
    private FreightRequest freightRequest;
    private DriverOffer driverOffer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Order.OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Order.OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public FreightRequest getFreightRequest() {
        return freightRequest;
    }

    public void setFreightRequest(FreightRequest freightRequest) {
        this.freightRequest = freightRequest;
    }

    public DriverOffer getDriverOffer() {
        return driverOffer;
    }

    public void setDriverOffer(DriverOffer driverOffer) {
        this.driverOffer = driverOffer;
    }


}
