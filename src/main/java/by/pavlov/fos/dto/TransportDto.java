package by.pavlov.fos.dto;

import by.pavlov.fos.validation.MinDoubleValue;
import by.pavlov.fos.validation.NotEmpty;
import by.pavlov.fos.validation.ValidEntity;

@ValidEntity("transportDto")
public class TransportDto {

    @NotEmpty
    private String name;
    @MinDoubleValue(0.5D)
    private Double maxWeight;
    @MinDoubleValue(0.1D)
    private Double maxVolume;
    private String description;
    @NotEmpty
    private byte[] photo;
    @NotEmpty
    private Long userAccountId;

    public TransportDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Double getMaxVolume() {
        return maxVolume;
    }

    public void setMaxVolume(Double maxVolume) {
        this.maxVolume = maxVolume;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }


}
