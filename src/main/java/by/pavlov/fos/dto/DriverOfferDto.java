package by.pavlov.fos.dto;

import by.pavlov.fos.validation.NotEmpty;
import by.pavlov.fos.validation.ValidEntity;

import java.math.BigDecimal;

@ValidEntity("driverOfferDto")
public class DriverOfferDto {

    @NotEmpty
    private BigDecimal price;
    private String offerDescription;
    @NotEmpty
    private Long transportId;
    @NotEmpty
    private Long freightRequestId;
    @NotEmpty
    private Long userAccountId;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public Long getTransportId() {
        return transportId;
    }

    public void setTransportId(Long transportId) {
        this.transportId = transportId;
    }

    public Long getFreightRequestId() {
        return freightRequestId;
    }

    public void setFreightRequestId(Long freightRequestId) {
        this.freightRequestId = freightRequestId;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long user_account_id) {
        this.userAccountId = user_account_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DriverOfferDto)) return false;

        DriverOfferDto that = (DriverOfferDto) o;

        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (offerDescription != null ? !offerDescription.equals(that.offerDescription) : that.offerDescription != null)
            return false;
        if (transportId != null ? !transportId.equals(that.transportId) : that.transportId != null) return false;
        if (freightRequestId != null ? !freightRequestId.equals(that.freightRequestId) : that.freightRequestId != null)
            return false;
        return userAccountId != null ? userAccountId.equals(that.userAccountId) : that.userAccountId == null;
    }

    @Override
    public int hashCode() {
        int result = price != null ? price.hashCode() : 0;
        result = 31 * result + (offerDescription != null ? offerDescription.hashCode() : 0);
        result = 31 * result + (transportId != null ? transportId.hashCode() : 0);
        result = 31 * result + (freightRequestId != null ? freightRequestId.hashCode() : 0);
        result = 31 * result + (userAccountId != null ? userAccountId.hashCode() : 0);
        return result;
    }
}
