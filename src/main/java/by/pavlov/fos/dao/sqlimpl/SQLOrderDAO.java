package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.DriverOfferConverter;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.OrderDAO;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.DriverOffer;
import by.pavlov.fos.entity.FreightRequest;
import by.pavlov.fos.entity.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SQLOrderDAO implements OrderDAO {


    private static final String TABLE_NAME = "final_order";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (status, freight_request_id, driver_offer_id) VALUES(?,?,?)";
    //TODO: change later
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET status=?, freight_request_id=?, driver_offer_id=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
    private static final String SELECT_ORDER_WITH_DETAILS_BY_USER_ID = "SELECT  final_order.*," +
            "freight_request.*," +
            "        driver_offer.*" +
            "        FROM final_order" +
            "        JOIN driver_offer ON final_order.driver_offer_id = driver_offer.id" +
            "        JOIN freight_request ON final_order.freight_request_id = freight_request.id WHERE freight_request.user_account_id=? AND final_order.status=?";
    private static final String SELECT_ORDER_WITH_DETAILS_BY_DRIVER_ID = "SELECT  final_order.*," +
            "freight_request.*," +
            "        driver_offer.*" +
            "        FROM final_order" +
            "        JOIN freight_request ON final_order.freight_request_id = freight_request.id " +
            "        JOIN driver_offer ON final_order.driver_offer_id = driver_offer.id WHERE driver_offer.user_account_id=? AND final_order.status=?";
    private static final String SELECT_ORDER_WITH_DETAILS_BY_ID_AND_NOT_EQUALS_STATUS = "SELECT  final_order.*," +
            "freight_request.*," +
            "        driver_offer.*" +
            "        FROM final_order" +
            "        JOIN freight_request ON final_order.freight_request_id = freight_request.id " +
            "        JOIN driver_offer ON final_order.driver_offer_id = driver_offer.id WHERE freight_request.user_account_id=? AND final_order.status <>?";
    private static final String SELECT_ORDER_WITH_DETAILS_BY_DRIVER_AND_NOT_EQUALS_STATUS = "SELECT  final_order.*," +
            "freight_request.*," +
            "        driver_offer.*" +
            "        FROM final_order" +
            "        JOIN freight_request ON final_order.freight_request_id = freight_request.id " +
            "        JOIN driver_offer ON final_order.driver_offer_id = driver_offer.id WHERE driver_offer.user_account_id=? AND final_order.status <>?";
    private static final String SELECT_BY_ID_WITH_JOIN = " SELECT  final_order.*," +
            "freight_request.*," +
            "        driver_offer.*" +
            "        FROM final_order" +
            "        JOIN freight_request ON final_order.freight_request_id = freight_request.id " +
            "        JOIN driver_offer ON final_order.driver_offer_id = driver_offer.id WHERE final_order.id=?";

    private final ConnectionManager connectionManager;
    private final EntityConverter<Order> orderEntityConverter;
    private final SQLCRUDHandler<Order> crudHandler;

    public SQLOrderDAO(ConnectionManager connectionManager, EntityConverter<Order> orderEntityConverter) {
        this.connectionManager = connectionManager;
        this.orderEntityConverter = orderEntityConverter;
        this.crudHandler = new SQLCRUDHandler<>(this.connectionManager, this.orderEntityConverter);
    }

    @Override
    public List<OrderDto> findAllOrdersByUserIdAndStatus(Long userId, String orderStatus) throws DAOException {

        return findAllByUserIdAndStatus(userId, orderStatus, SELECT_ORDER_WITH_DETAILS_BY_USER_ID);
    }

    @Override
    public List<OrderDto> findAllOrdersByDriverIdAndStatus(Long driverId, String orderStatus) throws DAOException {

        return findAllByUserIdAndStatus(driverId, orderStatus, SELECT_ORDER_WITH_DETAILS_BY_DRIVER_ID);
    }

    @Override
    public List<OrderDto> findAllOrdersByUserIdAndNotEqualsStatus(Long userId, String notEqualStatus) throws DAOException {

        return findAllByUserIdAndStatus(userId, notEqualStatus, SELECT_ORDER_WITH_DETAILS_BY_ID_AND_NOT_EQUALS_STATUS);
    }

    @Override
    public List<OrderDto> findAllOrdersByDriverIdAndNotEqualsStatus(Long userId, String notEqualStatus) throws DAOException {

        return findAllByUserIdAndStatus(userId, notEqualStatus, SELECT_ORDER_WITH_DETAILS_BY_DRIVER_AND_NOT_EQUALS_STATUS);
    }

    @Override
    public OrderDto findByIdAndCreateDto(Long orderId) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID_WITH_JOIN)) {
            preparedStatement.setLong(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<OrderDto> orderDtoList = buildOrderDtoList(resultSet);
            if (!orderDtoList.isEmpty()) {
                return orderDtoList.get(0);
            }
            return null;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Order> findAll() throws DAOException {
        return crudHandler.findAll(SELECT_ALL);
    }

    @Override
    public Order getById(Long id) throws DAOException {
        return crudHandler.getById(id, SELECT_BY_ID);
    }

    @Override
    public Long save(Order entity) throws DAOException {

        return crudHandler.save(entity, INSERT);
    }

    @Override
    public boolean update(Order entity) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getOrderStatus().name());
            preparedStatement.setLong(2, entity.getFreightId());
            preparedStatement.setLong(3, entity.getDriverOfferId());
            preparedStatement.setLong(4, entity.getId());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public boolean delete(Long id) throws DAOException {

        return crudHandler.delete(id, DELETE);
    }

    private List<OrderDto> findAllByUserIdAndStatus(Long userId, String status, String query) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setString(2, status);
            ResultSet resultSet = preparedStatement.executeQuery();
            return buildOrderDtoList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    private List<OrderDto> buildOrderDtoList(ResultSet resultSet) throws SQLException {

        List<OrderDto> orderDtoList = new ArrayList<>();
        while (resultSet.next()) {
            OrderDto orderDto = new OrderDto();
            orderDto.setId(resultSet.getLong("id"));
            orderDto.setOrderStatus(Order.OrderStatus.valueOf(resultSet.getString("status")));

            //get freight request
            FreightRequest freightRequest = new FreightRequest();
            freightRequest.setId(resultSet.getLong("id"));
            freightRequest.setCargoWeight(resultSet.getDouble("cargo_weight"));
            freightRequest.setCargoVolume(resultSet.getDouble("cargo_volume"));
            Timestamp timestamp = resultSet.getTimestamp("freight_date");
            freightRequest.setFreightDate(getCalendar(timestamp));
            freightRequest.setRequestStatus(FreightRequest
                    .RequestStatus
                    .valueOf(resultSet.getString("freight_status")));
            freightRequest.setDescription(resultSet.getString("cargo_description"));
            freightRequest.setUserAccountId(resultSet.getLong("user_account_id"));
            freightRequest.setRouteId(resultSet.getLong("route_id"));
            resultSet.getLong("route_id");
            //get driver offer
            DriverOffer driverOffer = new DriverOffer();
            driverOffer.setId(resultSet.getLong(DriverOfferConverter.BuildInfo.ID.getColumnName()));
            driverOffer.setPrice(resultSet.getBigDecimal(DriverOfferConverter.BuildInfo.PRICE.getColumnName()));
            driverOffer.setOfferDescription(resultSet.getString(DriverOfferConverter.BuildInfo.OFFER_DESCRIPTION.getColumnName()));
            driverOffer.setOfferStatus(DriverOffer.OfferStatus.valueOf(resultSet.getString(DriverOfferConverter.BuildInfo.OFFER_STATUS.getColumnName())));
            driverOffer.setTransportId(resultSet.getLong(DriverOfferConverter.BuildInfo.TRANSPORT.getColumnName()));
            driverOffer.setFreightRequestId(resultSet.getLong(DriverOfferConverter.BuildInfo.FREIGHT_REQUEST.getColumnName()));
            driverOffer.setUserAccountId(resultSet.getLong(DriverOfferConverter.BuildInfo.USER_ACCOUNT.getColumnName()));

            orderDto.setFreightRequest(freightRequest);
            orderDto.setDriverOffer(driverOffer);

            orderDtoList.add(orderDto);
        }
        return orderDtoList;
    }

    private Calendar getCalendar(Timestamp timestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.getTime());
        return calendar;
    }


}
