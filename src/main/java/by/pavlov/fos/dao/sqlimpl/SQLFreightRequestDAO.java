package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.FreightRequestDAO;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.entity.FreightRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SQLFreightRequestDAO implements FreightRequestDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLFreightRequestDAO.class);

    private static final String TABLE_NAME = "freight_request";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (cargo_weight, cargo_volume, freight_date, freight_status, cargo_description, user_account_id, route_id) VALUES(?,?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET cargo_weight=?, cargo_volume=?, freight_date=?, " +
            "freight_status=?, cargo_description=?, user_account_id=?, route_id=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
    private static final String SELECT_ALL_OPEN_REQUEST = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".freight_status=?";
    private static final String SELECT_ALL_BY_USER_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".user_account_id=? AND " + TABLE_NAME + ".freight_status=?";

    private final ConnectionManager connectionManager;
    private final SQLCRUDHandler<FreightRequest> crudHandler;
    private final EntityConverter<FreightRequest> entityConverter;

    public SQLFreightRequestDAO(ConnectionManager connectionManager, EntityConverter<FreightRequest> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(connectionManager, entityConverter);
    }

    @Override
    public List<FreightRequest> findAll() throws DAOException {

        return crudHandler.findAll(SELECT_ALL);
    }

    @Override
    public FreightRequest getById(Long id) throws DAOException {

        return crudHandler.getById(id, SELECT_BY_ID);
    }

    @Override
    public Long save(FreightRequest entity) throws DAOException {


        return crudHandler.save(entity, INSERT);
    }

    @Override
    public boolean update(FreightRequest entity) throws DAOException {

       return crudHandler.update(entity, entity.getId(), UPDATE);
    }

    @Override
    public boolean delete(Long id) throws DAOException {

       return crudHandler.delete(id, DELETE);
    }

    @Override
    public List<FreightRequest> findAllOpenRequest() throws DAOException {

        try (Connection connection = this.connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_OPEN_REQUEST)) {
            preparedStatement.setString(1, FreightRequest.RequestStatus.OPEN.name());
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting open requests ", e);
            throw new DAOException(e);
        }
    }


    @Override
    public List<FreightRequest> findAllByUserIdAndStatus(Long userId, FreightRequest.RequestStatus requestStatus) throws DAOException {

        try (Connection connection = this.connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BY_USER_ID)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setString(2, requestStatus.name());
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting user requests ", e);
            throw new DAOException(e);
        }

    }
}
