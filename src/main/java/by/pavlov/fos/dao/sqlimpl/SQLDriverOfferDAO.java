package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.DriverOfferDAO;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.entity.DriverOffer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class SQLDriverOfferDAO implements DriverOfferDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLDriverOfferDAO.class);

    private static final String TABLE_NAME = "driver_offer";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (price, offer_description, offer_status, transport_id, freight_request_id, user_account_id) VALUES(?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET price=?, offer_description=?, offer_status=?, transport_id=?, " +
            "freight_request_id=?, user_account_id=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
    private static final String SELECT_BY_USER_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".user_account_id=?";
    private static final String SELECT_BY_REQUEST_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".freight_request_id=?";
    private static final String DELETE_ALL_BY_REQUEST_ID_AND_STATUS = "DELETE FROM " + TABLE_NAME + " WHERE " + TABLE_NAME +".freight_request_id=? AND " + TABLE_NAME +".offer_status=?";
    private static final String SELECT_BY_REQUEST_ID_AND_USER_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".user_account_id=? AND " + TABLE_NAME + ".freight_request_id=?";

    private final ConnectionManager connectionManager;
    private final EntityConverter<DriverOffer> entityConverter;
    private final SQLCRUDHandler<DriverOffer> crudHandler;

    public SQLDriverOfferDAO(ConnectionManager connectionManager, EntityConverter<DriverOffer> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(this.connectionManager, this.entityConverter);
    }

    @Override
    public List<DriverOffer> findAll() throws DAOException {

        return crudHandler.findAll(SELECT_ALL);
    }

    @Override
    public DriverOffer getById(Long id) throws DAOException {

        return crudHandler.getById(id, SELECT_BY_ID);
    }

    @Override
    public Long save(DriverOffer entity) throws DAOException {

        return crudHandler.save(entity, INSERT);
    }

    @Override
    public boolean update(DriverOffer entity) throws DAOException {

        return crudHandler.update(entity,entity.getId(), UPDATE);
    }

    @Override
    public boolean delete(Long id) throws DAOException {

        return crudHandler.delete(id, DELETE);
    }

    @Override
    public boolean deleteAllByRequestIdAndStatus(Long requestId, DriverOffer.OfferStatus offerStatus) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_BY_REQUEST_ID_AND_STATUS)) {
            preparedStatement.setLong(1, requestId);
            preparedStatement.setString(2, offerStatus.name());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error("Error occurred while deleting offers", e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<DriverOffer> findAllByUserId(Long userId) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_USER_ID)) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting offer by user id", e);
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<DriverOffer> findByRequestIdAndUserId(Long userId, Long requestId) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_REQUEST_ID_AND_USER_ID)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, requestId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return  entityConverter.convertToEntities(resultSet).stream().findFirst();
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting offer by request and user id", e);
            throw new DAOException(e);
        }

    }

    @Override
    public List<DriverOffer> findAllByRequestId(Long requestId) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_REQUEST_ID)) {
            preparedStatement.setLong(1, requestId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting offer by request id", e);
            throw new DAOException(e);
        }
    }
}
