package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.RouteDAO;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.entity.Route;
import by.pavlov.fos.entity.RoutePoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLRouteDAO implements RouteDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLRouteDAO.class);

    private static final String TABLE_NAME = "route";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME + " (route) VALUES ('r')";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET description=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";

    private static final String INSERT_ROUTE_POINTS = "INSERT INTO route_point (coordinates, route_id) VALUES(?,?)";
    private static final String SELECT_ROUTE_POINTS = "SELECT * FROM route_point WHERE route_point.route_id=?";

    private final ConnectionManager connectionManager;
    private final EntityConverter<Route> entityConverter;
    private final SQLCRUDHandler<Route> crudHandler;

    public SQLRouteDAO(ConnectionManager connectionManager, EntityConverter<Route> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(connectionManager, entityConverter);
    }

    @Override
    public List<Route> findAll() throws DAOException {

        List<Route> routes = crudHandler.findAll(SELECT_ALL);
        for (Route route : routes) {
            route.setRoutePoints(selectRoutePoints(route.getId()));
        }
        return routes;
    }

    @Override
    public Route getById(Long id) throws DAOException {

        Route route = crudHandler.getById(id, SELECT_BY_ID);
        route.setRoutePoints(selectRoutePoints(route.getId()));
        return route;
    }

    @Override
    public Long save(Route entity) throws DAOException {

        Long id = crudHandler.save(entity, INSERT);
        entity.getRoutePoints().forEach(routePoint -> routePoint.setRouteId(id));
        saveRoutePoints(entity.getRoutePoints());
        return id;
    }

    @Override
    public boolean update(Route entity) throws DAOException {

        //TODO: update route points
        return crudHandler.update(entity, entity.getId(), UPDATE);
    }

    @Override
    public boolean delete(Long id) throws DAOException {

        //TODO: delete route points
        return crudHandler.delete(id, DELETE);
    }

    private void saveRoutePoints(List<RoutePoint> routePoints) {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROUTE_POINTS)) {
            for (RoutePoint routePoint : routePoints) {
                int i = 0;
                preparedStatement.setString(++i, routePoint.getCoordinates());
                preparedStatement.setLong(++i, routePoint.getRouteId());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<RoutePoint> selectRoutePoints(Long routeId) throws DAOException {

        List<RoutePoint> points = new ArrayList<>();
        try (Connection connection = this.connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROUTE_POINTS)) {
            preparedStatement.setLong(1, routeId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    RoutePoint routePoint = new RoutePoint();
                    routePoint.setRouteId(resultSet.getLong("id"));
                    routePoint.setCoordinates(resultSet.getString("coordinates"));
                    routePoint.setRouteId(resultSet.getLong("route_id"));
                    points.add(routePoint);

                }
                return points;
            }
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting route points ", e);
            throw new DAOException(e);
        }
    }


}
