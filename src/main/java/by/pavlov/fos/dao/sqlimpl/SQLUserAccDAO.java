package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.converter.UserRoleConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.dao.UserAccDAO;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SQLUserAccDAO implements UserAccDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLUserAccDAO.class);

    private static final String TABLE_NAME = "user_account";
    private static final String SELECT_BY_EMAIL = "SELECT * FROM " + TABLE_NAME + " WHERE user_account.email=?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE user_account.id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (email,first_name,last_name,password,phone_number,is_active,wallet_id) VALUES(?,?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET email=?, first_name=?, last_name=?, " +
            "password=?, phone_number=?, is_active=?, wallet_id=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";


    private static final String INSERT_ACCOUNT_ROLES = "INSERT INTO user_account_role (user_account_id,user_role_id) VALUES(?,?)";
    private static final String SELECT_USER_ROLES = "SELECT user_role.id, user_role.role_name FROM user_role JOIN user_account_role " +
            "ON user_role.id = user_account_role.user_role_id WHERE user_account_role.user_account_id = ?";
    private static final String SELECT_WALLET_ID = "SELECT wallet_id FROM " + TABLE_NAME + " WHERE user_account.id=?";


    private final ConnectionManager connectionManager;
    private final EntityConverter<UserAccount> entityConverter;
    private final UserRoleConverter userRoleConverter = new UserRoleConverter();
    private final SQLCRUDHandler<UserAccount> crudHandler;


    public SQLUserAccDAO(ConnectionManager connectionManager, EntityConverter<UserAccount> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(connectionManager, entityConverter);
    }


    @Override
    public UserAccount getByEmail(String email) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_EMAIL)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<UserAccount> foundUser = entityConverter.convertToEntities(resultSet);
            if (!foundUser.isEmpty()) {
                UserAccount userAccount = foundUser.get(0);
                List<UserRole> userRoles = selectUserRoles(connection, userAccount);
                userAccount.setUserRoles(userRoles);
                return foundUser.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<UserAccount> findAll() throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {

            ResultSet resultSet = preparedStatement.executeQuery();
            List<UserAccount> foundUsers = entityConverter.convertToEntities(resultSet);
            for (UserAccount userAccount : foundUsers) {
                List<UserRole> userRoles = selectUserRoles(connection, userAccount);
                userAccount.setUserRoles(userRoles);
            }
            return foundUsers;
        } catch (SQLException | DAOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public UserAccount getById(Long id) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<UserAccount> foundUser = entityConverter.convertToEntities(resultSet);
            if (!foundUser.isEmpty()) {
                UserAccount userAccount = foundUser.get(0);
                List<UserRole> userRoles = selectUserRoles(connection, userAccount);
                userAccount.setUserRoles(userRoles);
                return foundUser.get(0);
            } else {
                return null;
            }
        } catch (SQLException | DAOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }


    @Override
    public Long save(UserAccount entity) throws DAOException {
        try (Connection connection = connectionManager.getConnection()) {
            Long id = crudHandler.save(entity, INSERT);
            entity.setId(id);
            for (UserRole userRole : entity.getUserRoles()) {
                saveAccountRoles(connection, userRole, entity.getId());
            }
            return id;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public boolean update(UserAccount entity) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            return entityConverter.convertFromEntity(entity, preparedStatement).executeUpdate() > 0;
            //TODO: set roles?
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public boolean delete(Long id) throws DAOException {

        return crudHandler.delete(id, DELETE);
    }

    @Override
    public Long findUserWalletId(Long userId) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_WALLET_ID)) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("wallet_id");
            }
            return null;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }


    }

    private List<UserRole> selectUserRoles(Connection connection, UserAccount userAccount) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_ROLES)) {
            preparedStatement.setLong(1, userAccount.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            return userRoleConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }


    private void saveAccountRoles(Connection connection, UserRole userRole, Long userId) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ACCOUNT_ROLES)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, userRole.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }
}
