package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.dao.WalletDAO;
import by.pavlov.fos.entity.Wallet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class SQLWalletDAO  implements WalletDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLWalletDAO.class);

    private static final String TABLE_NAME = "wallet";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (amount) VALUES(?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET amount=? " +
            " WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";

    private final ConnectionManager connectionManager;
    private final EntityConverter<Wallet> entityConverter;
    private final SQLCRUDHandler<Wallet> crudHandler;

    public SQLWalletDAO(ConnectionManager connectionManager, EntityConverter<Wallet> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(connectionManager, entityConverter);
    }


    @Override
    public List<Wallet> findAll() throws DAOException {

        return crudHandler.findAll(SELECT_ALL);
    }

    @Override
    public Wallet getById(Long id) throws DAOException {

        return crudHandler.getById(id, SELECT_BY_ID);
    }

    @Override
    public Long save(Wallet entity) throws DAOException {

        return crudHandler.save(entity, INSERT);
    }

    @Override
    public boolean update(Wallet entity) throws DAOException {

        return crudHandler.update(entity,entity.getId(),UPDATE);

    }

    @Override
    public boolean delete(Long id) throws DAOException {

       return crudHandler.delete(id,DELETE);
    }
}
