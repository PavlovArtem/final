package by.pavlov.fos.dao.sqlimpl;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.SQLCRUDHandler;
import by.pavlov.fos.dao.TransportDAO;
import by.pavlov.fos.entity.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SQLTransportDAO implements TransportDAO {

    private static final Logger LOGGER = LogManager.getLogger(SQLTransportDAO.class);

    private static final String TABLE_NAME = "transport";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME +".id=?";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME +
            " (transport_name, max_weight, max_volume, description, transport_photo, user_account_id) VALUES(?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE " + TABLE_NAME + " SET name=?, max_weight=?, max_volume=?, " +
            "description=?, transport_photo=?, user_account_id=? WHERE id=?";
    private static final String DELETE = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
    private static final String SELECT_BY_USER_ID = "SELECT * FROM " + TABLE_NAME + " WHERE " + TABLE_NAME + ".user_account_id=?";

    private final ConnectionManager connectionManager;
    private final EntityConverter<Transport> entityConverter;
    private final SQLCRUDHandler<Transport> crudHandler;

    public SQLTransportDAO(ConnectionManager connectionManager, EntityConverter<Transport> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
        this.crudHandler = new SQLCRUDHandler<>(this.connectionManager,this.entityConverter);
    }

    @Override
    public List<Transport> findAll() throws DAOException {

        return crudHandler.findAll(SELECT_ALL);
    }

    @Override
    public Transport getById(Long id) throws DAOException {

        return crudHandler.getById(id, SELECT_BY_ID);
    }

    @Override
    public Long save(Transport entity) throws DAOException {

        return crudHandler.save(entity, INSERT);
    }

    @Override
    public boolean update(Transport entity) throws DAOException {

        return crudHandler.update(entity,entity.getId(), UPDATE);
    }

    @Override
    public boolean delete(Long id) throws DAOException {

        return crudHandler.delete(id, DELETE);
    }

    @Override
    public List<Transport> findAllByUserId(Long userId) throws DAOException {

        try(Connection connection = connectionManager.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_USER_ID)){
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error("Error occurred while selecting transport by user id",e);
            throw new DAOException(e);
        }
    }
}
