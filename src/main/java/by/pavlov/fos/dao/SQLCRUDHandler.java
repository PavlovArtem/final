package by.pavlov.fos.dao;

import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.converter.EntityConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;

public class SQLCRUDHandler<ENTITY> {

    private static final Logger LOGGER = LogManager.getLogger(SQLCRUDHandler.class);

    private final ConnectionManager connectionManager;
    private final EntityConverter<ENTITY> entityConverter;

    public SQLCRUDHandler(ConnectionManager connectionManager, EntityConverter<ENTITY> entityConverter) {
        this.connectionManager = connectionManager;
        this.entityConverter = entityConverter;
    }


    public List<ENTITY> findAll(String query) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            return entityConverter.convertToEntities(resultSet);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }


    public ENTITY getById(Long id, String query) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<ENTITY> entities = entityConverter.convertToEntities(resultSet);
            if (!entities.isEmpty()) {
                return entities.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }


    public Long save(ENTITY entity, String query) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            PreparedStatement executedPreparedStatement = entityConverter.convertFromEntity(entity, preparedStatement);
            executedPreparedStatement.executeUpdate();
            ResultSet generatedId = preparedStatement.getGeneratedKeys();
            generatedId.next();
            Long id = generatedId.getLong(1);
            executedPreparedStatement.close();
            return id;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }

    public boolean update(ENTITY entity, Long id, String query) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            PreparedStatement executeStatement = entityConverter.convertFromEntity(entity, preparedStatement);
            int parametersCounter = executeStatement.getParameterMetaData().getParameterCount();
            executeStatement.setLong(parametersCounter, id);
            return executeStatement.executeUpdate() > 0;

        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }


    public boolean delete(Long id, String query) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            throw new DAOException(e.getMessage(), e);
        }
    }
}
