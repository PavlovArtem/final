package by.pavlov.fos.dao;

import by.pavlov.fos.entity.UserAccount;

public interface UserAccDAO extends CRUDDao<UserAccount, Long> {

    UserAccount getByEmail(String email) throws DAOException;

    Long findUserWalletId(Long userId) throws DAOException;

}
