package by.pavlov.fos.dao;

import by.pavlov.fos.entity.Wallet;

public interface WalletDAO extends CRUDDao<Wallet,Long> {
}
