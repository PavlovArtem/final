package by.pavlov.fos.dao;

import by.pavlov.fos.entity.Route;

public interface RouteDAO extends CRUDDao<Route, Long> {
}
