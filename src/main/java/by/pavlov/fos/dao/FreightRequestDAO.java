package by.pavlov.fos.dao;

import by.pavlov.fos.entity.FreightRequest;

import java.util.List;

public interface FreightRequestDAO extends CRUDDao<FreightRequest, Long> {

    List<FreightRequest> findAllOpenRequest() throws DAOException;

    List<FreightRequest> findAllByUserIdAndStatus(Long userId, FreightRequest.RequestStatus requestStatus ) throws DAOException;
}
