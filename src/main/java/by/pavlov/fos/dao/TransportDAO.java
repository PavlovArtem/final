package by.pavlov.fos.dao;

import by.pavlov.fos.entity.Transport;

import java.util.List;

public interface TransportDAO extends CRUDDao<Transport, Long> {

    List<Transport> findAllByUserId(Long userId) throws DAOException;

}
