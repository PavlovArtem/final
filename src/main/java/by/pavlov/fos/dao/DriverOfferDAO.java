package by.pavlov.fos.dao;

import by.pavlov.fos.entity.DriverOffer;

import java.util.List;
import java.util.Optional;

public interface DriverOfferDAO extends CRUDDao<DriverOffer, Long> {

    Optional<DriverOffer> findByRequestIdAndUserId(Long userId, Long requestId) throws DAOException;

    List<DriverOffer> findAllByUserId(Long userId) throws DAOException;

    List<DriverOffer> findAllByRequestId(Long requestId) throws DAOException;

    boolean deleteAllByRequestIdAndStatus(Long requestId, DriverOffer.OfferStatus offerStatus) throws DAOException;

}
