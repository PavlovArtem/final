package by.pavlov.fos.dao;

import java.util.List;

public interface CRUDDao<T,K> {
    

    List<T> findAll() throws DAOException;

    T getById(K id) throws DAOException;

    K save(T entity) throws DAOException;

    boolean update(T entity) throws DAOException;

    boolean delete(K id) throws DAOException;



}
