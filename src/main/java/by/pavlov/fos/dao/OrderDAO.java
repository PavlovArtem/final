package by.pavlov.fos.dao;

import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.Order;

import java.util.List;

public interface OrderDAO extends CRUDDao<Order, Long> {


    List<OrderDto> findAllOrdersByUserIdAndStatus(Long userId, String orderStatus) throws DAOException;

    List<OrderDto> findAllOrdersByDriverIdAndStatus(Long driverId, String orderStatus) throws DAOException;

    List<OrderDto> findAllOrdersByUserIdAndNotEqualsStatus(Long userId, String notEqualStatus) throws DAOException;

    List<OrderDto> findAllOrdersByDriverIdAndNotEqualsStatus(Long userId, String notEqualStatus) throws DAOException;

    OrderDto findByIdAndCreateDto(Long orderId) throws DAOException;
}
