package by.pavlov.fos.converter;

import by.pavlov.fos.entity.FreightRequest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FreightRequestConverter implements EntityConverter<FreightRequest> {
    @Override
    public List<FreightRequest> convertToEntities(ResultSet resultSet) throws SQLException {

        List<FreightRequest> freightRequests = new ArrayList<>();
        while (resultSet.next()) {
            FreightRequest freightRequest = new FreightRequest();
            freightRequest.setId(resultSet.getLong(BuildInfo.ID.columnName));
            freightRequest.setCargoWeight(resultSet.getDouble(BuildInfo.CARGO_WEIGHT.columnName));
            freightRequest.setCargoVolume(resultSet.getDouble(BuildInfo.CARGO_VOLUME.columnName));
            Timestamp timestamp = resultSet.getTimestamp(BuildInfo.FREIGHT_DATE.columnName);
            freightRequest.setFreightDate(getCalendar(timestamp));
            freightRequest.setRequestStatus(FreightRequest
                    .RequestStatus
                    .valueOf(resultSet.getString(BuildInfo.REQUEST_STATUS.columnName)));
            freightRequest.setDescription(resultSet.getString(BuildInfo.CARGO_DESCRIPTION.columnName));
            freightRequest.setUserAccountId(resultSet.getLong(BuildInfo.USER_ACCOUNT_ID.columnName));
            freightRequest.setRouteId(resultSet.getLong(BuildInfo.ROUTE_ID.columnName));
            freightRequests.add(freightRequest);
        }
        return freightRequests;
    }

    private Calendar getCalendar(Timestamp timestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.getTime());
        return calendar;
    }

    @Override
    public PreparedStatement convertFromEntity(FreightRequest entity, PreparedStatement preparedStatement) throws SQLException {

        int i = 0;
        preparedStatement.setDouble(++i, entity.getCargoWeight());
        preparedStatement.setDouble(++i, entity.getCargoVolume());
        Timestamp timestamp = new Timestamp(entity.getFreightDate().getTimeInMillis());
        preparedStatement.setTimestamp(++i, timestamp);
        preparedStatement.setString(++i, entity.getRequestStatus().name());
        preparedStatement.setString(++i, entity.getDescription());
        preparedStatement.setLong(++i, entity.getUserAccountId());
        preparedStatement.setLong(++i, entity.getRouteId());
        return preparedStatement;
    }

    private enum BuildInfo {

        ID("id"),
        CARGO_WEIGHT("cargo_weight"),
        CARGO_VOLUME("cargo_volume"),
        FREIGHT_DATE("freight_date"),
        REQUEST_STATUS("freight_status"),
        CARGO_DESCRIPTION("cargo_description"),
        USER_ACCOUNT_ID("user_account_id"),
        ROUTE_ID("route_id");

        private final String columnName;

        BuildInfo(String columnName) {
            this.columnName = columnName;
        }

    }

}
