package by.pavlov.fos.converter;

import by.pavlov.fos.entity.UserAccount;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserAccountConverter implements EntityConverter<UserAccount> {


    @Override
    public List<UserAccount> convertToEntities(ResultSet resultSet) throws SQLException {
        List<UserAccount> users = new ArrayList<>();
        while (resultSet.next()) {
            UserAccount userToAdd = new UserAccount();
            userToAdd.setId(resultSet.getLong(BuildInfo.ID.columnName));
            userToAdd.setEmail(resultSet.getString(BuildInfo.EMAIL.columnName));
            userToAdd.setName(resultSet.getString(BuildInfo.FIRST_NAME.columnName));
            userToAdd.setLastName(resultSet.getString(BuildInfo.LAST_NAME.columnName));
            userToAdd.setPassword(resultSet.getString(BuildInfo.PASSWORD.columnName));
            userToAdd.setPhone(resultSet.getString(BuildInfo.PHONE_NUMBER.columnName));
            userToAdd.setActive(resultSet.getBoolean(BuildInfo.IS_ACTIVE.columnName));
            userToAdd.setWalletId(resultSet.getLong(BuildInfo.WALLET_ID.columnName));

            users.add(userToAdd);
        }

        return users;
    }

    @Override
    public PreparedStatement convertFromEntity(UserAccount entity, PreparedStatement preparedStatement) throws SQLException {
        int i = 0;
        preparedStatement.setString(++i, entity.getEmail());
        preparedStatement.setString(++i, entity.getName());
        preparedStatement.setString(++i, entity.getLastName());
        preparedStatement.setString(++i, entity.getPassword());
        preparedStatement.setString(++i, entity.getPhone());
        preparedStatement.setBoolean(++i, entity.isActive());
        preparedStatement.setLong(++i, entity.getWalletId());

        return preparedStatement;
    }


    private enum BuildInfo {
        ID("id"),
        EMAIL("email"),
        FIRST_NAME("first_name"),
        LAST_NAME("last_name"),
        PASSWORD("password"),
        PHONE_NUMBER("phone_number"),
        IS_ACTIVE("is_active"),
        WALLET_ID("wallet_id");

        private final String columnName;

        BuildInfo(String columnName) {
            this.columnName = columnName;
        }

    }

}
