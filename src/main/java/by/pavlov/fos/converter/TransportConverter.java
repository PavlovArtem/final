package by.pavlov.fos.converter;

import by.pavlov.fos.entity.Transport;

import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransportConverter implements EntityConverter<Transport> {


    @Override
    public List<Transport> convertToEntities(ResultSet resultSet) throws SQLException {
        List<Transport> transportList = new ArrayList<>();
        while (resultSet.next()){
            Transport transport = new Transport();
            transport.setId(resultSet.getLong(BuildInfo.ID.columnName));
            transport.setName(resultSet.getString(BuildInfo.NAME.columnName));
            transport.setMaxWeight(resultSet.getDouble(BuildInfo.MAX_WEIGHT.columnName));
            transport.setMaxVolume(resultSet.getDouble(BuildInfo.MAX_VOLUME.columnName));
            transport.setDescription(resultSet.getString(BuildInfo.DESCRIPTION.columnName));
            Blob blob = resultSet.getBlob(BuildInfo.PHOTO.columnName);
            transport.setPhoto(getPhoto(blob));
            transport.setUserAccountId(resultSet.getLong(BuildInfo.USER_ACCOUNT_ID.columnName));
            transportList.add(transport);
        }
        return transportList;
    }

    private byte[] getPhoto(Blob blob) throws SQLException {
        int blobLength = (int) blob.length();
        byte[] blobAsBytes = blob.getBytes(1, blobLength);
        blob.free();
        return blobAsBytes;
    }

    @Override
    public PreparedStatement convertFromEntity(Transport entity, PreparedStatement preparedStatement) throws SQLException {
        int i = 0;
        preparedStatement.setString(++i, entity.getName());
        preparedStatement.setDouble(++i, entity.getMaxWeight());
        preparedStatement.setDouble(++i, entity.getMaxVolume());
        preparedStatement.setString(++i, entity.getDescription());
        preparedStatement.setBlob(++i, new ByteArrayInputStream(entity.getPhoto()));
        preparedStatement.setLong(++i, entity.getUserAccountId());
        return preparedStatement;
    }


    private enum BuildInfo{

        ID("id"),
        NAME("transport_name"),
        MAX_WEIGHT("max_weight"),
        MAX_VOLUME("max_volume"),
        DESCRIPTION("description"),
        PHOTO("transport_photo"),
        USER_ACCOUNT_ID("user_account_id");

        private String columnName;

        BuildInfo(String columnName) {
            this.columnName = columnName;
        }

    }
}
