package by.pavlov.fos.converter;

import by.pavlov.fos.entity.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRoleConverter implements EntityConverter<UserRole> {

    //TODO: make it simple
    @Override
    public List<UserRole> convertToEntities(ResultSet resultSet) throws SQLException {
        List<UserRole> roles = new ArrayList<>();
        while (resultSet.next()){
            UserRole userRole = UserRole.valueOf(resultSet
                    .getString("role_name")
                    .toUpperCase());
            roles.add(userRole);
        }
        return roles;
    }

    @Override
    public PreparedStatement convertFromEntity(UserRole entity, PreparedStatement preparedStatement) throws SQLException {
        int i = 0;
        preparedStatement.setString(++i,entity.name());
        return preparedStatement;
    }
}
