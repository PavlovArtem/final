package by.pavlov.fos.converter;

import by.pavlov.fos.entity.DriverOffer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DriverOfferConverter implements EntityConverter<DriverOffer> {


    @Override
    public List<DriverOffer> convertToEntities(ResultSet resultSet) throws SQLException {

        List<DriverOffer> driverOfferList = new ArrayList<>();
        while (resultSet.next()){
            DriverOffer driverOffer = new DriverOffer();
            driverOffer.setId(resultSet.getLong(BuildInfo.ID.columnName));
            driverOffer.setPrice(resultSet.getBigDecimal(BuildInfo.PRICE.columnName));
            driverOffer.setOfferDescription(resultSet.getString(BuildInfo.OFFER_DESCRIPTION.columnName));
            driverOffer.setOfferStatus(DriverOffer.OfferStatus.valueOf(resultSet.getString(BuildInfo.OFFER_STATUS.columnName)));
            driverOffer.setTransportId(resultSet.getLong(BuildInfo.TRANSPORT.columnName));
            driverOffer.setFreightRequestId(resultSet.getLong(BuildInfo.FREIGHT_REQUEST.columnName));
            driverOffer.setUserAccountId(resultSet.getLong(BuildInfo.USER_ACCOUNT.columnName));
            driverOfferList.add(driverOffer);
        }
        return driverOfferList;
    }

    @Override
    public PreparedStatement convertFromEntity(DriverOffer entity, PreparedStatement preparedStatement) throws SQLException {

        int i = 0;
        preparedStatement.setBigDecimal(++i, entity.getPrice());
        preparedStatement.setString(++i, entity.getOfferDescription());
        preparedStatement.setString(++i, entity.getOfferStatus().name());
        preparedStatement.setLong(++i, entity.getTransportId());
        preparedStatement.setLong(++i, entity.getFreightRequestId());
        preparedStatement.setLong(++i, entity.getUserAccountId());
        return preparedStatement;
    }

    public enum  BuildInfo{

        ID("id"),
        PRICE("price"),
        OFFER_DESCRIPTION("offer_description"),
        OFFER_STATUS("offer_status"),
        TRANSPORT("transport_id"),
        FREIGHT_REQUEST("freight_request_id"),
        USER_ACCOUNT("user_account_id");

        private String columnName;


        public String getColumnName(){
            return this.columnName;
        }

        BuildInfo(String columnName) {
            this.columnName = columnName;
        }
    }
}
