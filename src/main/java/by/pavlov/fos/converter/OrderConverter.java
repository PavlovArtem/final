package by.pavlov.fos.converter;

import by.pavlov.fos.entity.Order;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderConverter implements EntityConverter<Order> {

    @Override
    public List<Order> convertToEntities(ResultSet resultSet) throws SQLException {

        List<Order> orders = new ArrayList<>();
        while (resultSet.next()){
            Order order = new Order();
            order.setId(resultSet.getLong("id"));
            order.setOrderStatus(Order.OrderStatus.valueOf(resultSet.getString("status")));
            order.setFreightId(resultSet.getLong("freight_request_id"));
            order.setDriverOfferId(resultSet.getLong("driver_offer_id"));
            orders.add(order);
        }
        return orders;
    }

    @Override
    public PreparedStatement convertFromEntity(Order entity, PreparedStatement preparedStatement) throws SQLException {

        int i = 0;
        preparedStatement.setString(++i, entity.getOrderStatus().name());
        preparedStatement.setLong(++i, entity.getFreightId());
        preparedStatement.setLong(++i, entity.getDriverOfferId());
        return preparedStatement;
    }
}
