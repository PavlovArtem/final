package by.pavlov.fos.converter;

import by.pavlov.fos.entity.Route;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoteConverter implements EntityConverter<Route> {

    @Override
    public List<Route> convertToEntities(ResultSet resultSet) throws SQLException {

        List<Route> routes = new ArrayList<>();
        while (resultSet.next()){
            Route route = new Route();
            route.setId(resultSet.getLong("id"));
            routes.add(route);
        }

        return routes;
    }

    @Override
    public PreparedStatement convertFromEntity(Route entity, PreparedStatement preparedStatement) throws SQLException {

        return preparedStatement;
    }
}
