package by.pavlov.fos.converter;

import by.pavlov.fos.entity.Wallet;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WalletConverter implements EntityConverter<Wallet> {


    //TODO: finish signUp
    @Override
    public List<Wallet> convertToEntities(ResultSet resultSet) throws SQLException {
        List<Wallet> wallets = new ArrayList<>();
        while (resultSet.next()){
            Wallet wallet = new Wallet();
            wallet.setId(resultSet.getLong("id"));
            wallet.setAmount(resultSet.getBigDecimal("amount"));

            wallets.add(wallet);
        }
        return wallets;
    }

    @Override
    public PreparedStatement convertFromEntity(Wallet entity, PreparedStatement preparedStatement) throws SQLException {
        int i = 0;
        preparedStatement.setBigDecimal(++i,entity.getAmount());
        return preparedStatement;
    }
}
