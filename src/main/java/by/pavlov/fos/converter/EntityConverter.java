package by.pavlov.fos.converter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @param <T> any entity to convert
 */
public interface EntityConverter<T> {

    List<T> convertToEntities(ResultSet resultSet) throws SQLException;

    PreparedStatement convertFromEntity(T entity, PreparedStatement preparedStatement) throws SQLException;



}
