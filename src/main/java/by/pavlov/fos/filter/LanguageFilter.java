package by.pavlov.fos.filter;

import by.pavlov.fos.application.ApplicationConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import static by.pavlov.fos.application.ApplicationConstants.*;

@WebFilter(servletNames = {"index"}, filterName = "lang_filter")
public class LanguageFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {

            String lang = request.getParameter(ApplicationConstants.PARAM_LANGUAGE);
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            if (("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang))) {
                Cookie langCookie = new Cookie(ApplicationConstants.PARAM_LANGUAGE, lang);
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute(ApplicationConstants.PARAM_LANGUAGE, lang);
                ((HttpServletResponse) response).addCookie(langCookie);
            } else {
                Optional<Cookie[]> cookies = Optional.ofNullable(httpRequest.getCookies());
                Cookie langCookie = cookies.map(Stream::of).orElse(Stream.empty())
                        .filter(cookie -> cookie.getName().equalsIgnoreCase(ApplicationConstants.PARAM_LANGUAGE)).findFirst()
                        .orElse(new Cookie(ApplicationConstants.PARAM_LANGUAGE, "en"));
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute(ApplicationConstants.PARAM_LANGUAGE, langCookie.getValue());
                ((HttpServletResponse) response).addCookie(langCookie);
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
