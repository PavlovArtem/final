package by.pavlov.fos.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@WebFilter(servletNames = {"index"}, filterName = "ce_filter")
public class CharacterEncodingFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        String characterEncoding = servletRequest.getCharacterEncoding();
        String utf8 = StandardCharsets.UTF_8.name();
        if (!utf8.equalsIgnoreCase(characterEncoding)) {
            servletRequest.setCharacterEncoding(utf8);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
