package by.pavlov.fos.filter;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.SecurityContext;
import by.pavlov.fos.command.CommandType;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter(servletNames = {"index"}, filterName = "security")
public class CommandSecurityFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.setCurrentSessionId(request.getSession().getId());
        String commandName = request.getParameter(ApplicationConstants.COMMAND_PARAM);

        Optional<CommandType> commandType = CommandType.of(commandName);
        if (commandType.isPresent() && securityContext.canExecute(commandType.get())) {
            filterChain.doFilter(request, servletResponse);
        } else if (!commandType.isPresent()) {
            filterChain.doFilter(request, servletResponse);
        } else {
            ((HttpServletResponse) servletResponse).sendRedirect("?" + ApplicationConstants.COMMAND_PARAM + " = " + CommandType.ERROR_COMMAND);
        }

    }

    @Override
    public void destroy() {

    }
}
