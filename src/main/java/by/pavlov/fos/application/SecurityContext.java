package by.pavlov.fos.application;

import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.entity.UserAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SecurityContext {

    private static final Logger LOGGER = LogManager.getLogger(SecurityContext.class);

    private static SecurityContext instance;
    private static final AtomicBoolean IS_EXIST = new AtomicBoolean(false);
    private static final Lock INSTANCE_LOCK = new ReentrantLock();
    private final ThreadLocal<String> currentSessionIdStorage = new ThreadLocal<>();
    private final Map<String, UserAccount> userAccountMap = new ConcurrentHashMap<>(1000);
    private Properties properties = new Properties();

    private SecurityContext() {
        init();
    }

    public static SecurityContext getInstance() {

        if (!IS_EXIST.get()) {
            INSTANCE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new SecurityContext();
                    IS_EXIST.set(true);
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    private void init() {
        try (InputStream propertyStream = SecurityContext.class.getResourceAsStream("/security.properties")) {
            properties.load(propertyStream);
        } catch (IOException e) {
            LOGGER.error("Failed to read properties", e);
            throw new IllegalStateException("Failed to read security properties", e);
        }
    }

    public String getCurrentSessionId() {
        return currentSessionIdStorage.get();
    }

    public void login(UserAccount userAccount, String sessionId) {
        userAccountMap.put(sessionId, userAccount);
    }

    public void setCurrentSessionId(String sessionId) {
        currentSessionIdStorage.set(sessionId);
    }

    public UserAccount getCurrentUser() {

        String currentSessionId = getCurrentSessionId();
        return currentSessionId != null ? userAccountMap.get(currentSessionId) : null;
    }

    public boolean canExecute(CommandType commandType) {
        UserAccount userAccount = getCurrentUser();
        return canExecute(userAccount, commandType);
    }

    public boolean canExecute(UserAccount userAccount, CommandType commandType) {

        String commandToRoles = properties.getProperty("command." + commandType.name());
        List<String> roles = Optional.ofNullable(commandToRoles)
                .map(s -> Arrays.asList(s.split(",")))
                .orElseGet(ArrayList::new);

        boolean hasPermission = userAccount != null && roles.stream()
                .anyMatch(role -> userHasRole(userAccount, role));

        return hasPermission || roles.isEmpty();

    }

    private boolean userHasRole(UserAccount userAccount, String role) {
        return userAccount.getUserRoles()
                .stream()
                .anyMatch(userRole -> userRole.name().equalsIgnoreCase(role));
    }

    public boolean isLoggedIn() {
        return getCurrentUser() != null;
    }

    public void logout(String sessionId) {
        userAccountMap.remove(sessionId);
    }


}
