package by.pavlov.fos.application;

import by.pavlov.fos.command.AbstractCommand;
import by.pavlov.fos.command.BasicCommandProvider;
import by.pavlov.fos.command.CommandProvider;
import by.pavlov.fos.command.CommandType;
import by.pavlov.fos.command.impl.AdminDashboardCommand;
import by.pavlov.fos.command.impl.DisplayDriverPageCommand;
import by.pavlov.fos.command.impl.DisplayWalletPageCommand;
import by.pavlov.fos.command.impl.FillUpWalletCommand;
import by.pavlov.fos.command.impl.IndexCommand;
import by.pavlov.fos.command.impl.LogOutCommand;
import by.pavlov.fos.command.impl.LoginCommand;
import by.pavlov.fos.command.impl.OfferAcceptCommand;
import by.pavlov.fos.command.impl.OfferCreateCommand;
import by.pavlov.fos.command.impl.OffersDisplayCommand;
import by.pavlov.fos.command.impl.OrderCancelCommand;
import by.pavlov.fos.command.impl.OrderFinishCommand;
import by.pavlov.fos.command.impl.OrderStartExecutionCommand;
import by.pavlov.fos.command.impl.OrdersDisplayFinishedCommand;
import by.pavlov.fos.command.impl.OrdersDisplayUnfinishedCommand;
import by.pavlov.fos.command.impl.RequestCancelCommand;
import by.pavlov.fos.command.impl.RequestCreateCommand;
import by.pavlov.fos.command.impl.RequestsDisplayCommand;
import by.pavlov.fos.command.impl.SignUpCommand;
import by.pavlov.fos.command.impl.TransportCreateCommand;
import by.pavlov.fos.command.impl.TransportDeleteCommand;
import by.pavlov.fos.command.impl.TransportDisplayCommand;
import by.pavlov.fos.command.impl.TransportDisplayUpdateCommand;
import by.pavlov.fos.connection.ConnectionManager;
import by.pavlov.fos.connection.ConnectionManagerImpl;
import by.pavlov.fos.connection.ConnectionPool;
import by.pavlov.fos.connection.ConnectionPoolImpl;
import by.pavlov.fos.connection.TransactionManager;
import by.pavlov.fos.connection.TransactionManagerImpl;
import by.pavlov.fos.connection.Transactional;
import by.pavlov.fos.converter.DriverOfferConverter;
import by.pavlov.fos.converter.FreightRequestConverter;
import by.pavlov.fos.converter.OrderConverter;
import by.pavlov.fos.converter.RoteConverter;
import by.pavlov.fos.converter.TransportConverter;
import by.pavlov.fos.converter.UserAccountConverter;
import by.pavlov.fos.converter.WalletConverter;
import by.pavlov.fos.dao.DriverOfferDAO;
import by.pavlov.fos.dao.FreightRequestDAO;
import by.pavlov.fos.dao.OrderDAO;
import by.pavlov.fos.dao.RouteDAO;
import by.pavlov.fos.dao.TransportDAO;
import by.pavlov.fos.dao.UserAccDAO;
import by.pavlov.fos.dao.WalletDAO;
import by.pavlov.fos.dao.sqlimpl.SQLDriverOfferDAO;
import by.pavlov.fos.dao.sqlimpl.SQLFreightRequestDAO;
import by.pavlov.fos.dao.sqlimpl.SQLOrderDAO;
import by.pavlov.fos.dao.sqlimpl.SQLRouteDAO;
import by.pavlov.fos.dao.sqlimpl.SQLTransportDAO;
import by.pavlov.fos.dao.sqlimpl.SQLUserAccDAO;
import by.pavlov.fos.dao.sqlimpl.SQLWalletDAO;
import by.pavlov.fos.service.ClientFreightService;
import by.pavlov.fos.service.DriverFreightService;
import by.pavlov.fos.service.OrderService;
import by.pavlov.fos.service.TransportService;
import by.pavlov.fos.service.UserAccountService;
import by.pavlov.fos.service.WalletService;
import by.pavlov.fos.service.impl.ClientFreightServiceImpl;
import by.pavlov.fos.service.impl.DriverFreightServiceImpl;
import by.pavlov.fos.service.impl.OrderServiceImpl;
import by.pavlov.fos.service.impl.UserAccServiceImpl;
import by.pavlov.fos.service.impl.UserTransportServiceImpl;
import by.pavlov.fos.service.impl.WalletServiceImpl;
import by.pavlov.fos.validation.EntityValidator;
import by.pavlov.fos.validation.FieldValidator;
import by.pavlov.fos.validation.IsEmailValid;
import by.pavlov.fos.validation.MinDoubleValue;
import by.pavlov.fos.validation.MinLength;
import by.pavlov.fos.validation.impl.AnnotationBasedValidator;
import by.pavlov.fos.validation.impl.EmailValidator;
import by.pavlov.fos.validation.impl.MinDoubleValueValidator;
import by.pavlov.fos.validation.impl.MinLengthValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);
    private static final Lock INSTANCE_LOCK = new ReentrantLock();
    private static final AtomicBoolean IS_EXIST = new AtomicBoolean(false);
    private static ApplicationContext instance;
    private final Map<Class<?>, Object> beans = new HashMap<>();


    private ApplicationContext() {
        init();
    }

    public static ApplicationContext getInstance() {

        if (!IS_EXIST.get()) {
            INSTANCE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ApplicationContext();
                    IS_EXIST.set(true);
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;

    }

    private static <T> InvocationHandler createTransactionalInvocationHandler(TransactionManager tm, T service) {
        return ((proxy, method, args) -> {

            Method declaredMethod = service.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
            if (method.isAnnotationPresent(Transactional.class)
                    || declaredMethod.isAnnotationPresent(Transactional.class)) {
                tm.beginTransaction();
                try {
                    Object result = method.invoke(service, args);
                    tm.commitTransaction();
                    return result;
                } catch (Exception e) {
                    LOGGER.error("Transaction will be rollback methodName={}", method.getName());
                    tm.rollbackTransaction();
                    throw e;
                }
            } else {
                return method.invoke(service, args);
            }
        });
    }

    private static <T> T createProxy(ClassLoader classLoader, InvocationHandler invocationHandler, Class<T>... toBeProxied) {
        return (T) Proxy.newProxyInstance(classLoader, toBeProxied, invocationHandler);
    }

    private void init() {

        // create connectionPool
        ConnectionPool connectionPool = new ConnectionPoolImpl();
        TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
        ConnectionManager connectionManager = new ConnectionManagerImpl(connectionPool, transactionManager);

        beans.put(ConnectionPool.class, connectionPool);
        beans.put(TransactionManager.class, transactionManager);
        beans.put(ConnectionManager.class, connectionManager);


        // DAO
        UserAccDAO userAccDAO = new SQLUserAccDAO(connectionManager, new UserAccountConverter());
        WalletDAO walletDAO = new SQLWalletDAO(connectionManager, new WalletConverter());
        RouteDAO routeDAO = new SQLRouteDAO(connectionManager, new RoteConverter());
        FreightRequestDAO freightRequestDAO = new SQLFreightRequestDAO(connectionManager, new FreightRequestConverter());
        TransportDAO transportDAO = new SQLTransportDAO(connectionManager, new TransportConverter());
        DriverOfferDAO driverOfferDAO = new SQLDriverOfferDAO(connectionManager, new DriverOfferConverter());
        OrderDAO orderDAO = new SQLOrderDAO(connectionManager, new OrderConverter());

        beans.put(UserAccDAO.class, userAccDAO);
        beans.put(WalletDAO.class, walletDAO);
        beans.put(RouteDAO.class, routeDAO);
        beans.put(FreightRequestDAO.class, freightRequestDAO);
        beans.put(TransportDAO.class, transportDAO);
        beans.put(DriverOfferDAO.class, driverOfferDAO);
        beans.put(OrderDAO.class, orderDAO);


        // SERVICE
        UserAccountService userAccountService = new UserAccServiceImpl(userAccDAO, walletDAO);
        ClientFreightService clientFreightService = new ClientFreightServiceImpl(freightRequestDAO, routeDAO, userAccDAO, transportDAO, driverOfferDAO);
        TransportService transportService = new UserTransportServiceImpl(transportDAO);
        DriverFreightService driverFreightService = new DriverFreightServiceImpl(clientFreightService, driverOfferDAO, freightRequestDAO, transportDAO, routeDAO, userAccDAO);
        WalletService walletService = new WalletServiceImpl(walletDAO, userAccDAO);
        OrderService orderService = new OrderServiceImpl(orderDAO, walletService, freightRequestDAO, driverOfferDAO, userAccDAO);

        // Proxy SERVICE
        InvocationHandler userAccountServiceHandler = createTransactionalInvocationHandler(transactionManager, userAccountService);
        UserAccountService userAccountServiceProxy = createProxy(getClass().getClassLoader(), userAccountServiceHandler, UserAccountService.class);
        InvocationHandler clientFreightServiceHandler = createTransactionalInvocationHandler(transactionManager, clientFreightService);
        ClientFreightService clientFreightServiceProxy = createProxy(getClass().getClassLoader(), clientFreightServiceHandler, ClientFreightService.class);
        InvocationHandler transportInvocationHandler = createTransactionalInvocationHandler(transactionManager, transportService);
        TransportService transportServiceProxy = createProxy(getClass().getClassLoader(), transportInvocationHandler, TransportService.class);
        InvocationHandler driverRequestServiceHandler = createTransactionalInvocationHandler(transactionManager, driverFreightService);
        DriverFreightService driverFreightServiceProxy = createProxy(getClass().getClassLoader(), driverRequestServiceHandler, DriverFreightService.class);
        InvocationHandler walletServiceHandler = createTransactionalInvocationHandler(transactionManager, walletService);
        WalletService walletServiceProxy = createProxy(getClass().getClassLoader(), walletServiceHandler, WalletService.class);
        InvocationHandler orderServiceHandler = createTransactionalInvocationHandler(transactionManager, orderService);
        OrderService orderServiceProxy = createProxy(getClass().getClassLoader(), orderServiceHandler, OrderService.class);

        beans.put(UserAccountService.class, userAccountServiceProxy);
        beans.put(ClientFreightService.class, clientFreightServiceProxy);
        beans.put(TransportService.class, transportServiceProxy);
        beans.put(DriverFreightService.class, driverFreightServiceProxy);
        beans.put(WalletService.class, walletServiceProxy);
        beans.put(OrderService.class, orderServiceProxy);

        //Validation
        Map<Class<? extends Annotation>, FieldValidator> validatorMap = new HashMap<>();
        validatorMap.put(IsEmailValid.class, new EmailValidator());
        validatorMap.put(MinDoubleValue.class, new MinDoubleValueValidator());
        validatorMap.put(MinLength.class, new MinLengthValidator());
        EntityValidator entityValidator = new AnnotationBasedValidator(validatorMap);

        beans.put(EntityValidator.class, entityValidator);


        //commands
        CommandProvider commandProvider = new BasicCommandProvider();
        //admin commands
        commandProvider.register(CommandType.DISPLAY_ADMIN_DASHBOARD_COMMAND, new AdminDashboardCommand(userAccountServiceProxy));
        //common commands
        commandProvider.register(CommandType.INDEX_COMMAND, new IndexCommand());
        commandProvider.register(CommandType.ERROR_COMMAND, new AbstractCommand() {
            @Override
            protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
                request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
            }
        });

        commandProvider.register(CommandType.SIGN_UP_DISPLAY_COMMAND, new AbstractCommand() {
            @Override
            protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
                forward(request, response, "signUp");
            }
        });
        commandProvider.register(CommandType.SIGN_UP_COMMAND, new SignUpCommand(userAccountServiceProxy, entityValidator));

        commandProvider.register(CommandType.LOGIN_DISPLAY_COMMAND, new AbstractCommand() {
            @Override
            protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
                forward(request, response, "login");
            }
        });
        commandProvider.register(CommandType.LOGIN_COMMAND, new LoginCommand(userAccountServiceProxy));
        commandProvider.register(CommandType.FILL_UP_WALLET_COMMAND, new FillUpWalletCommand(walletServiceProxy));
        commandProvider.register(CommandType.DISPLAY_WALLET_PAGE_COMMAND, new DisplayWalletPageCommand(walletServiceProxy));
        commandProvider.register(CommandType.LOG_OUT_COMMAND, new LogOutCommand());
        commandProvider.register(CommandType.DISPLAY_UNFINISHED_ORDERS_COMMAND, new OrdersDisplayUnfinishedCommand(orderServiceProxy));
        commandProvider.register(CommandType.DISPLAY_FINISHED_ORDERS, new OrdersDisplayFinishedCommand(orderServiceProxy));
        commandProvider.register(CommandType.CANCEL_ORDER_COMMAND, new OrderCancelCommand(orderServiceProxy));
        //client commands
        commandProvider.register(CommandType.REQUEST_CANCEL_COMMAND, new RequestCancelCommand(clientFreightServiceProxy));
        commandProvider.register(CommandType.START_ORDER_EXECUTION_COMMAND, new OrderStartExecutionCommand(orderServiceProxy));
        commandProvider.register(CommandType.ACCEPT_OFFER_COMMAND, new OfferAcceptCommand(clientFreightServiceProxy, driverFreightServiceProxy, orderServiceProxy, walletServiceProxy));
        commandProvider.register(CommandType.CREATE_FREIGHT_REQUEST, new RequestCreateCommand(clientFreightServiceProxy, entityValidator));

        commandProvider.register(CommandType.SHOW_OFFERS_COMMAND, new OffersDisplayCommand(clientFreightServiceProxy));
        commandProvider.register(CommandType.SHOW_CLIENT_OPEN_REQUESTS_COMMAND, new RequestsDisplayCommand(clientFreightServiceProxy));
        //driver commands
        commandProvider.register(CommandType.FINISH_ORDER_COMMAND, new OrderFinishCommand(orderServiceProxy));
        commandProvider.register(CommandType.TRANSPORT_DELETE_COMMAND, new TransportDeleteCommand(transportServiceProxy));
        commandProvider.register(CommandType.SHOW_TRANSPORT_COMMAND, new TransportDisplayCommand(transportServiceProxy));
        commandProvider.register(CommandType.TRANSPORT_DISPLAY_CREATION_COMMAND, new AbstractCommand() {
            @Override
            protected void executeWrapped(HttpServletRequest request, HttpServletResponse response) throws Exception {
                canExecute(request, response, CommandType.TRANSPORT_DISPLAY_CREATION_COMMAND);
                forward(request, response, "transport/create_transport");
            }
        });
        commandProvider.register(CommandType.TRANSPORT_DISPLAY_UPDATE_COMMAND, new TransportDisplayUpdateCommand(transportServiceProxy));
        commandProvider.register(CommandType.CREATE_TRANSPORT_COMMAND, new TransportCreateCommand(transportServiceProxy, entityValidator));
        commandProvider.register(CommandType.DISPLAY_DRIVER_PAGE_COMMAND, new DisplayDriverPageCommand(transportServiceProxy, driverFreightServiceProxy));
        commandProvider.register(CommandType.CREATE_DRIVER_OFFER_COMMAND, new OfferCreateCommand(driverFreightServiceProxy, transportServiceProxy, entityValidator));


        //beans
        beans.put(CommandProvider.class, commandProvider);

    }

    public void destroy() {

        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }

}
