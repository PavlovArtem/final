package by.pavlov.fos.application;

public class ApplicationConstants {

    //app constants
    public static final String COMMAND_PARAM = "_command";
    public static final String DATABASE_PROPERTIES_PATH = "database.properties";
    public static final String PARAM_LANGUAGE = "lang";

    //jsp
    public static final String ERROR_PAGE = "error";
    public static final String MAIN_PAGE = "index";
    public static final String SIGN_UP_PAGE = "signUp";
    public static final String LOG_IN_PAGE = "login";
    public static final String CREATE_REQUEST = "create_request";
    public static final String DRIVER_REQUEST_PAGE = "offer/driver_request_page";
    public static final String REQUEST_PAGE = "request_page";
    public static final String REQUEST_CARD = "request/request_card";
    public static final String CREATE_TRANSPORT_PAGE = "transport/create_transport";
    public static final String SHOW_TRANSPORT_PAGE = "show_transport";

    //Freight request params
    public static final String CARGO_WEIGHT = "cargo_weight";
    public static final String CARGO_VOLUME = "cargo_volume";
    public static final String FREIGHT_DATE = "freight_date";
    public static final String FREIGHT_DESCRIPTION = "description";
    public static final String FREIGHT_ROUTE_POINTS = "route_points";
    public static final String PLACE_REQUEST = "place_request";

    public static final String FREIGHT_REQUEST_DTO = "freight_dto";
    public static final String MAIN_SERVLET_PATH = "/app";

    public static final String EMAIL_REGEX_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";


    //transport create params
    public static final String TRANSPORT_ID = "transport_id";
    public static final String TRANSPORT_IMAGE = "transport_image";
    public static final String TRANSPORT_NAME = "transport_name";
    public static final String TRANSPORT_MAX_WEIGHT = "transport_max_weight";
    public static final String TRANSPORT_MAX_VOLUME = "transport_max_volume";
    public static final String TRANSPORT_DESCRIPTION = "transport_description";

    public static final String TRANSPORT_LIST = "transport_list";
}
