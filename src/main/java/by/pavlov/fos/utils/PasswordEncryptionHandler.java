package by.pavlov.fos.utils;

import java.security.NoSuchAlgorithmException;

public interface PasswordEncryptionHandler {

    String encryptPassword(String originalPassword) throws NoSuchAlgorithmException;

}
