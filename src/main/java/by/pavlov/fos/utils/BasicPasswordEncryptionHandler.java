package by.pavlov.fos.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BasicPasswordEncryptionHandler implements PasswordEncryptionHandler {


    @Override
    public String encryptPassword(String originalPassword) throws NoSuchAlgorithmException {

        return toHexString(encrypt(originalPassword));
    }

    private byte[] encrypt(String password) {

        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(password.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }

    }

    private String toHexString(byte[] hash){

        BigInteger number = new BigInteger(1,hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));

        while (hexString.length() < 32){
            hexString.insert(0,'0');
        }
        return hexString.toString();

    }

}
