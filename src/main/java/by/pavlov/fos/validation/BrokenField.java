package by.pavlov.fos.validation;

public class BrokenField {

    private final String fieldName;
    private final Object fieldValue;
    private final String validationRule;
    private final Object[] args;

    public BrokenField(String fieldName, Object fieldValue, String validationRule, Object... args) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.validationRule = validationRule;
        this.args = args;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }

    public String getValidationRule() {
        return validationRule;
    }

    public Object[] getArgs() {
        return args;
    }
}
