package by.pavlov.fos.validation.impl;

import by.pavlov.fos.validation.BrokenField;
import by.pavlov.fos.validation.FieldValidator;
import by.pavlov.fos.validation.MinDoubleValue;
import by.pavlov.fos.validation.ValidationException;

import java.lang.reflect.Field;

public class MinDoubleValueValidator implements FieldValidator {
    @Override
    public BrokenField validateField(Object entity, Field field) {

        if (Double.class.equals(field.getType())){
            MinDoubleValue minDoubleValue = field.getAnnotation(MinDoubleValue.class);
            try{
                Double fieldValue = (Double) field.get(entity);
                if (fieldValue != null && fieldValue < minDoubleValue.value()){

                    return new BrokenField(field.getName(), fieldValue, "minDoubleValue", minDoubleValue.value());
                }
            } catch (IllegalAccessException e) {
                throw new ValidationException(e);
            }
        }
        return null;

    }
}
