package by.pavlov.fos.validation.impl;

import by.pavlov.fos.validation.BrokenField;
import by.pavlov.fos.validation.FieldValidator;
import by.pavlov.fos.validation.MinLength;
import by.pavlov.fos.validation.ValidationException;

import java.lang.reflect.Field;

public class MinLengthValidator implements FieldValidator {


    @Override
    public BrokenField validateField(Object entity, Field field) {

        if (String.class.equals(field.getType())){
            MinLength minLength = field.getAnnotation(MinLength.class);
            try{
                String fieldValue = (String) field.get(entity);
                if (field != null && fieldValue.trim().length() < minLength.value()){
                    return new BrokenField(field.getName(), fieldValue, "minLength", minLength.value());
                }
            } catch (IllegalAccessException e) {
                throw new ValidationException(e);
            }
        }

        return null;
    }
}
