package by.pavlov.fos.validation.impl;

import by.pavlov.fos.validation.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class AnnotationBasedValidator implements EntityValidator {

    private final Map<Class<? extends Annotation>, FieldValidator> validationMethods;
    private final Set<Class<? extends Annotation>> supportedAnnotations;

    public AnnotationBasedValidator(Map<Class<? extends Annotation>, FieldValidator> validationMethods) {
        this.validationMethods = validationMethods;
        this.supportedAnnotations = validationMethods.keySet();
    }


    @Override
    public ValidationResult validate(Object entity) {

        ValidationResult validationResult = new ValidationResult();

        if (entity == null) {
            throw new ValidationException("Passed entity is null");
        }

        Class<?> clazz = entity.getClass();
        if (!clazz.isAnnotationPresent(ValidEntity.class)) {
            String message = MessageFormat.format("{0} does not have the {1} annotation ",
                    clazz, ValidEntity.class.getName());
            throw new ValidationException(message);
        }

        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            List<BrokenField> brokenFields = supportedAnnotations.stream()
                    .filter(field::isAnnotationPresent)
                    .map(validationMethods::get)
                    .map(fieldValidator -> fieldValidator.validateField(entity, field))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            validationResult.addBrokenFields(brokenFields);
        }

        return validationResult;
    }
}
