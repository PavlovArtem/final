package by.pavlov.fos.validation.impl;

import by.pavlov.fos.validation.BrokenField;
import by.pavlov.fos.validation.FieldValidator;
import by.pavlov.fos.validation.ValidationException;

import java.lang.reflect.Field;
import java.util.Collection;

public class NotEmptyValidator implements FieldValidator {

    @Override
    public BrokenField validateField(Object entity, Field field) {

        if (Collection.class.isAssignableFrom(field.getType())) {
            try {
                Collection<?> fieldValue = (Collection<?>) field.get(entity);
                if (fieldValue == null || fieldValue.isEmpty()) {
                    return new BrokenField(field.getName(), fieldValue, "notEmpty");
                }
            } catch (IllegalAccessException e) {
                throw new ValidationException(e);
            }
        } else if (String.class.isAssignableFrom(field.getType())) {
            try {
                String fieldValue = (String) field.get(entity);
                if (fieldValue == null || fieldValue.isEmpty()) {
                    return new BrokenField(field.getName(), fieldValue, "notEmpty");
                }
            } catch (IllegalAccessException e) {
                throw new ValidationException(e);
            }
        }

        return null;

    }
}
