package by.pavlov.fos.validation.impl;

import by.pavlov.fos.validation.BrokenField;
import by.pavlov.fos.validation.FieldValidator;
import by.pavlov.fos.validation.IsEmailValid;
import by.pavlov.fos.validation.ValidationException;

import java.lang.reflect.Field;
import java.util.regex.Pattern;

public class EmailValidator implements FieldValidator {


    @Override
    public BrokenField validateField(Object entity, Field field) {

        if (String.class.isAssignableFrom(field.getType())) {
            IsEmailValid annotation = field.getAnnotation(IsEmailValid.class);
            String regex = annotation.regex();
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            try {
                String fieldValue = (String) field.get(entity);
                if (fieldValue != null
                        && !fieldValue.isEmpty()
                        && !pattern.matcher(fieldValue).find()) {
                    return new BrokenField(field.getName(), fieldValue, "email");
                }
            } catch (IllegalAccessException e) {
                throw new ValidationException(e);
            }
        }
        return null;
    }
}
