package by.pavlov.fos.validation;

import java.lang.reflect.Field;

public interface FieldValidator {

    BrokenField validateField(Object entity, Field field);
}
