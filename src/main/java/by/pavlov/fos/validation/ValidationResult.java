package by.pavlov.fos.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {

    private final List<BrokenField> brokenFields = new ArrayList<>();
    private final List<String> errorMessages = new ArrayList<>();

    public void addBrokenFields(List<BrokenField> brokenFields){
        this.brokenFields.addAll(brokenFields);
    }

    public List<BrokenField> getBrokenFields(){
        return new ArrayList<>(brokenFields);
    }

    public void addErrorMessage(String errorMessage){
        this.errorMessages.add(errorMessage);
    }

    public List<String> getErrorMessages(){
        brokenFieldsToErrorMessages();
        return this.errorMessages;
    }

    private void brokenFieldsToErrorMessages(){
        for (BrokenField field: this.brokenFields){
            StringBuilder sb = new StringBuilder();
            sb.append("error: ").append(field.getValidationRule());
            sb.append("value: ").append(field.getFieldValue());
            this.errorMessages.add(sb.toString());
        }
    }

}
