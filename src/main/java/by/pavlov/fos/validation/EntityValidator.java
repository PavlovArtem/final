package by.pavlov.fos.validation;

public interface EntityValidator {

    ValidationResult validate(Object entity);

}
