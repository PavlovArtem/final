package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dto.*;

import java.util.List;

public interface DriverFreightService {

    DisplayOfferDto getDriverOfferForRequest(Long userId, Long freightId) throws DAOException;

    List<FreightRequestDto> getAllOpenRequests() throws DAOException;

    boolean createOffer(DriverOfferDto driverOfferDto) throws DAOException;

    DisplayOfferDto getOfferById(Long offerId) throws DAOException;

    List<OrderDto> getAllUserOrders(Long userId);


}
