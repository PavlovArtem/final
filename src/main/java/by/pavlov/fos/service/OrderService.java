package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.UserAccount;

import java.util.List;

public interface OrderService {

    // 1 Client
    // 2 Driver
    List<OrderDto> findAllUnfinishedOrders(UserAccount userAccount) throws DAOException; //1.2.

    // 1.2
    List<OrderDto> findAllFinishedOrders(UserAccount userAccount) throws DAOException;

    // 1.2
    OrderDto findOrderById(Long orderId) throws DAOException;

    /**
     * Order CLIENT can create ORDERS
     * NOTE that CLIENT must have enough money on wallet account to
     * accept offer and create order
     * @param offerId id of accepted offer
     * @return true if order was created successfully
     */
    boolean confirmOfferAndCreateOrder(Long offerId) throws ServiceException;

    /**
     * Order can be canceled only if
     * Order.OrderStatus not equals IN_PROGRESS or FINISHED
     * @param orderId id of canceled order
     * @return true if order was canceled
     */
    boolean cancelOrder(Long orderId) throws DAOException;

    // 1

    /**
     * Changes order status to IN_PROGRESS
     * only user with role CLIENT can begin order
     * execution
     * @param orderId
     * @return
     */
    boolean beginFreight(Long orderId) throws DAOException;

    /**
     * Changes order status to FINISHED
     * only user with role DRIVER can finish order
     * @param orderId
     * @return
     */
    boolean finishFreight(Long orderId) throws DAOException;




}
