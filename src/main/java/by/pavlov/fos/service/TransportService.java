package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dto.ShowTransportDto;
import by.pavlov.fos.dto.TransportDto;

import java.util.List;

public interface TransportService {

    List<ShowTransportDto> findAllUserTransport(Long userId) throws DAOException;

    Long createTransport(TransportDto transportDto) throws DAOException;

    boolean updateTransport(TransportDto transportDto);

    ShowTransportDto getTransport(Long transportId) throws DAOException;

    boolean deleteTransport(Long transportId) throws DAOException;




}
