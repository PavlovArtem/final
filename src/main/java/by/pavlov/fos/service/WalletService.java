package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.entity.Wallet;

import java.math.BigDecimal;

public interface WalletService {

    Wallet getWallet(Long walletId) throws DAOException;

    boolean fillUpWallet(BigDecimal amount, Long walletId) throws DAOException;

    boolean isEnoughMoney(BigDecimal amount, Long walletId) throws DAOException;

    boolean withdrawMoney(BigDecimal amount, Long walletId) throws DAOException;

}
