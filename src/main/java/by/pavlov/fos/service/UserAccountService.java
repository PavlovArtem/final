package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.entity.UserAccount;


import java.util.List;
import java.util.Optional;

public interface UserAccountService {

    Optional<UserAccount> findByEmail(String email) throws ServiceException;

    Optional<UserAccount> findById(Long id) throws  DAOException;

    List<UserAccount> findAll() throws ServiceException;

    boolean signUp(UserAccount userAccount) throws ServiceException;

    UserAccount login(String email, String password) throws ServiceException;

}
