package by.pavlov.fos.service.impl;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.UserAccDAO;
import by.pavlov.fos.dao.WalletDAO;
import by.pavlov.fos.entity.Wallet;
import by.pavlov.fos.service.ServiceException;
import by.pavlov.fos.service.WalletService;

import java.math.BigDecimal;

public class WalletServiceImpl implements WalletService {

    private final WalletDAO walletDAO;
    private final UserAccDAO userAccDAO;

    public WalletServiceImpl(WalletDAO walletDAO, UserAccDAO userAccDAO) {
        this.walletDAO = walletDAO;
        this.userAccDAO = userAccDAO;
    }


    @Override
    public Wallet getWallet(Long walletId) throws DAOException {
        return walletDAO.getById(walletId);
    }

    @Override
    public boolean fillUpWallet(BigDecimal amount, Long walletId) throws DAOException {

        if (amount.compareTo(BigDecimal.valueOf(0.1d)) < 0){
            throw new IllegalArgumentException("amount can't be less than 0.1");
        }
        Wallet wallet = walletDAO.getById(walletId);
        BigDecimal oldAmount = wallet.getAmount();
        wallet.setAmount(oldAmount.add(amount));
        walletDAO.update(wallet);
        Wallet checkWallet = walletDAO.getById(walletId);
        return oldAmount.compareTo(checkWallet.getAmount()) < 0;
    }

    @Override
    public boolean isEnoughMoney(BigDecimal amount, Long walletId) throws DAOException {

        Wallet wallet = walletDAO.getById(walletId);
        BigDecimal value = wallet.getAmount();

        return value.compareTo(amount) >= 0;
    }

    @Override
    public boolean withdrawMoney(BigDecimal amount, Long walletId) throws DAOException {

        Wallet wallet = walletDAO.getById(walletId);
        BigDecimal walletAmount = wallet.getAmount();
        if (walletAmount.compareTo(amount) >= 0){
            wallet.setAmount(walletAmount.subtract(amount));
            walletDAO.update(wallet);
            return true;
        }
        return false;
    }
}
