package by.pavlov.fos.service.impl;

import by.pavlov.fos.connection.Transactional;
import by.pavlov.fos.dao.*;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.*;
import by.pavlov.fos.service.OrderService;
import by.pavlov.fos.service.ServiceException;
import by.pavlov.fos.service.WalletService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = LogManager.getLogger(OrderServiceImpl.class);

    private final OrderDAO orderDAO;
    private final WalletService walletService;
    private final FreightRequestDAO freightRequestDAO;
    private final DriverOfferDAO driverOfferDAO;
    private final UserAccDAO userAccDAO;

    public OrderServiceImpl(OrderDAO orderDAO, WalletService walletService, FreightRequestDAO freightRequestDAO, DriverOfferDAO driverOfferDAO, UserAccDAO userAccDAO) {
        this.orderDAO = orderDAO;
        this.walletService = walletService;
        this.freightRequestDAO = freightRequestDAO;
        this.driverOfferDAO = driverOfferDAO;
        this.userAccDAO = userAccDAO;
    }

    @Override
    public List<OrderDto> findAllUnfinishedOrders(UserAccount userAccount) throws DAOException {

        if (userAccount.getUserRoles().stream().anyMatch(userRole -> userRole.equals(UserRole.CLIENT))) {
            return orderDAO.findAllOrdersByUserIdAndNotEqualsStatus(userAccount.getId(), Order.OrderStatus.FINISHED.name());
        } else if (userAccount.getUserRoles().stream().anyMatch(userRole -> userRole.equals(UserRole.DRIVER))) {
            return orderDAO.findAllOrdersByDriverIdAndNotEqualsStatus(userAccount.getId(), Order.OrderStatus.FINISHED.name());
        }
        return new ArrayList<>();
    }

    @Override
    public List<OrderDto> findAllFinishedOrders(UserAccount userAccount) throws DAOException {

        if (userAccount.getUserRoles().stream().anyMatch(userRole -> userRole.equals(UserRole.CLIENT))) {
            return orderDAO.findAllOrdersByUserIdAndStatus(userAccount.getId(), Order.OrderStatus.FINISHED.name());
        } else if (userAccount.getUserRoles().stream().anyMatch(userRole -> userRole.equals(UserRole.DRIVER))) {
            return orderDAO.findAllOrdersByDriverIdAndStatus(userAccount.getId(), Order.OrderStatus.FINISHED.name());
        }
        return new ArrayList<>();
    }

    @Override
    public OrderDto findOrderById(Long orderId) throws DAOException {

        return orderDAO.findByIdAndCreateDto(orderId);
    }


    @Override
    @Transactional
    public boolean confirmOfferAndCreateOrder(Long offerId) throws ServiceException {

        try {
            DriverOffer driverOffer = driverOfferDAO.getById(offerId);
            FreightRequest freightRequest = freightRequestDAO.getById(driverOffer.getFreightRequestId());


            Long walletId = userAccDAO.findUserWalletId(freightRequest.getUserAccountId());
            if (!walletService.isEnoughMoney(driverOffer.getPrice(), walletId)) {
                throw new ServiceException("not enough money");
            }
            walletService.withdrawMoney(driverOffer.getPrice(), walletId);

            //update offer status
            driverOffer.setOfferStatus(DriverOffer.OfferStatus.ACCEPT);
            driverOfferDAO.update(driverOffer);
            //update freight request status
            freightRequest.setRequestStatus(FreightRequest.RequestStatus.CLOSE);
            freightRequestDAO.update(freightRequest);

            Order order = new Order();
            order.setOrderStatus(Order.OrderStatus.NOT_STARTED);
            order.setFreightId(driverOffer.getFreightRequestId());
            order.setDriverOfferId(driverOffer.getId());
            orderDAO.save(order);
            return true;
        } catch (DAOException e) {
            LOGGER.error("Error occurred while creating order", e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public boolean cancelOrder(Long orderId) throws DAOException {

        Order order = orderDAO.getById(orderId);
        FreightRequest freightRequest = freightRequestDAO.getById(order.getFreightId());
        DriverOffer driverOffer = driverOfferDAO.getById(order.getDriverOfferId());
        Long walletId = userAccDAO.findUserWalletId(freightRequest.getUserAccountId());
        walletService.fillUpWallet(driverOffer.getPrice(), walletId);
        order.setOrderStatus(Order.OrderStatus.CANCELED);
        return orderDAO.update(order);

    }

    @Override
    public boolean beginFreight(Long orderId) throws DAOException {

        Order order = orderDAO.getById(orderId);
        order.setOrderStatus(Order.OrderStatus.IN_PROGRESS);
        return orderDAO.update(order);

    }

    @Override
    @Transactional
    public boolean finishFreight(Long orderId) throws DAOException {


        Order order = orderDAO.getById(orderId);
        DriverOffer driverOffer = driverOfferDAO.getById(order.getDriverOfferId());
        Long walletId = userAccDAO.findUserWalletId(driverOffer.getUserAccountId());
        walletService.fillUpWallet(driverOffer.getPrice(), walletId);
        order.setOrderStatus(Order.OrderStatus.FINISHED);
        return orderDAO.update(order);

    }


}
