package by.pavlov.fos.service.impl;

import by.pavlov.fos.connection.Transactional;
import by.pavlov.fos.dao.*;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.entity.*;
import by.pavlov.fos.service.ClientFreightService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class ClientFreightServiceImpl implements ClientFreightService {

    private static final Logger LOGGER = LogManager.getLogger(ClientFreightServiceImpl.class);

    private final FreightRequestDAO freightRequestDAO;
    private final RouteDAO routeDAO;
    private final UserAccDAO userAccDAO;
    private final TransportDAO transportDAO;
    private final DriverOfferDAO driverOfferDAO;

    public ClientFreightServiceImpl(FreightRequestDAO freightRequestDAO, RouteDAO routeDAO, UserAccDAO userAccDAO, TransportDAO transportDAO, DriverOfferDAO driverOfferDAO) {
        this.freightRequestDAO = freightRequestDAO;
        this.routeDAO = routeDAO;
        this.userAccDAO = userAccDAO;
        this.transportDAO = transportDAO;
        this.driverOfferDAO = driverOfferDAO;
    }

    @Override
    @Transactional
    public Long createFreightRequest(FreightRequestDto freightRequestDto) throws DAOException {

        //TODO: add ckeck user role must be client

        Route route = new Route();
        List<RoutePoint> points = new ArrayList<>();
        for (String coordinate : freightRequestDto.getCoordinates()) {
            RoutePoint routePoint = new RoutePoint();
            routePoint.setCoordinates(coordinate);
            points.add(routePoint);
        }
        route.setRoutePoints(points);
        Long routeId = routeDAO.save(route);
        FreightRequest freightRequest = new FreightRequest();
        freightRequest.setRequestStatus(FreightRequest.RequestStatus.OPEN);
        freightRequest.setCargoWeight(freightRequestDto.getCargoWeight());
        freightRequest.setCargoVolume(freightRequestDto.getCargoVolume());
        freightRequest.setRouteId(routeId);
        freightRequest.setUserAccountId(freightRequestDto.getUserId());
        freightRequest.setFreightDate(freightRequestDto.getFreightDate());
        freightRequest.setDescription(freightRequestDto.getDescription());

        return freightRequestDAO.save(freightRequest);
    }

    @Override
    public FreightRequestDto getFreightRequest(Long id) throws DAOException {

        FreightRequest freightRequest = freightRequestDAO.getById(id);
        if (freightRequest != null){
            Route route = routeDAO.getById(freightRequest.getRouteId());
            return buildFreightRequestDTO(freightRequest, route);
        }
        return null;
    }

    @Override
    public List<FreightRequestDto> getAllOpenRequests() throws DAOException {

        List<FreightRequestDto> freightRequestDtoList = new ArrayList<>();
        for (FreightRequest freightRequest : freightRequestDAO.findAllOpenRequest()) {
            Route route = routeDAO.getById(freightRequest.getRouteId());
            freightRequestDtoList.add(buildFreightRequestDTO(freightRequest, route));
        }
        return freightRequestDtoList;

    }


    @Override
    public List<FreightRequestDto> getAllOpenUserRequests(Long userId) throws DAOException {

        List<FreightRequestDto> freightRequestDtoList = new ArrayList<>();
        for (FreightRequest freightRequest : freightRequestDAO.findAllByUserIdAndStatus(userId, FreightRequest.RequestStatus.OPEN)) {
            Route route = routeDAO.getById(freightRequest.getRouteId());
            freightRequestDtoList.add(buildFreightRequestDTO(freightRequest, route));
        }
        return freightRequestDtoList;
    }

    @Override
    public boolean deleteFreightRequest(Long id) throws DAOException {

        return freightRequestDAO.delete(id);

    }

    @Override
    public DriverOffer findOfferById(Long offerId) throws DAOException {

        return driverOfferDAO.getById(offerId);
    }

    @Override
    public List<DisplayOfferDto> getOffers(Long requestId) throws DAOException {

        List<DisplayOfferDto> displayOfferDtoList = new ArrayList<>();
        for (DriverOffer driverOffer : driverOfferDAO.findAllByRequestId(requestId)) {
            DisplayOfferDto displayOfferDto = new DisplayOfferDto();
            displayOfferDto.setPrice(driverOffer.getPrice());
            displayOfferDto.setDescription(driverOffer.getOfferDescription());
            displayOfferDto.setFreightId(driverOffer.getFreightRequestId());
            displayOfferDto.setOfferId(driverOffer.getId());
            byte[] photo = transportDAO.getById(driverOffer.getTransportId()).getPhoto();
            String base64Image = Base64.getEncoder().encodeToString(photo);
            displayOfferDto.setTransportEncodedPhoto(base64Image);
            displayOfferDtoList.add(displayOfferDto);
        }
        return displayOfferDtoList;
    }

    @Override
    public boolean deleteAllOffersByRequestId(Long id) throws DAOException {
        return driverOfferDAO.deleteAllByRequestIdAndStatus(id, DriverOffer.OfferStatus.WAITING);
    }


    private FreightRequestDto buildFreightRequestDTO(FreightRequest freightRequest, Route route) {


        FreightRequestDto freightRequestDto = new FreightRequestDto();
        freightRequestDto.setId(freightRequest.getId());
        freightRequestDto.setUserId(freightRequest.getUserAccountId());
        freightRequestDto.setCargoWeight(freightRequest.getCargoWeight());
        freightRequestDto.setCargoVolume(freightRequest.getCargoVolume());
        freightRequestDto.setFreightDate(freightRequest.getFreightDate());
        freightRequestDto.setRequestStatus(freightRequest.getRequestStatus());
        freightRequestDto.setDescription(freightRequest.getDescription());

        for (RoutePoint routePoint : route.getRoutePoints()) {
            freightRequestDto.getCoordinates().add(routePoint.getCoordinates());
        }

        return freightRequestDto;

    }


}
