package by.pavlov.fos.service.impl;

import by.pavlov.fos.connection.Transactional;
import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.UserAccDAO;
import by.pavlov.fos.dao.WalletDAO;
import by.pavlov.fos.entity.UserAccount;
import by.pavlov.fos.entity.Wallet;
import by.pavlov.fos.service.ServiceException;
import by.pavlov.fos.service.UserAccountService;
import by.pavlov.fos.utils.BasicPasswordEncryptionHandler;
import by.pavlov.fos.utils.PasswordEncryptionHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

public class UserAccServiceImpl implements UserAccountService {

    private static final Logger LOGGER = LogManager.getLogger(UserAccServiceImpl.class);

    private final UserAccDAO userAccDAO;
    private final WalletDAO walletDAO;
    private final PasswordEncryptionHandler passwordEncryptor = new BasicPasswordEncryptionHandler();

    public UserAccServiceImpl(UserAccDAO userAccDAO, WalletDAO walletDAO) {
        this.userAccDAO = userAccDAO;
        this.walletDAO = walletDAO;
    }


    @Override
    public Optional<UserAccount> findByEmail(String email) throws ServiceException {

        try {
            return Optional.ofNullable(userAccDAO.getByEmail(email));
        } catch (DAOException e) {
            LOGGER.error("Error occured while trying to get data", e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<UserAccount> findById(Long id) throws DAOException {

        return Optional.ofNullable(userAccDAO.getById(id));
    }

    @Override
    public List<UserAccount> findAll() throws ServiceException {

        try {
            return userAccDAO.findAll();
        } catch (DAOException e) {
            LOGGER.error("Error occured while trying to get data", e);
            throw new ServiceException(e.getMessage(), e);
        }

    }

    @Override
    @Transactional
    public boolean signUp(UserAccount userAccount) throws ServiceException {

        try {
            Wallet wallet = new Wallet();
            wallet.setAmount(BigDecimal.valueOf(0L));
            Long walletId = walletDAO.save(wallet);
            String encryptedPassword = passwordEncryptor.encryptPassword(userAccount.getPassword());
            userAccount.setPassword(encryptedPassword);
            userAccount.setWalletId(walletId);
            userAccount.setActive(true);
            userAccDAO.save(userAccount);
            return true;
        } catch (DAOException | NoSuchAlgorithmException e) {
            LOGGER.error("Error occured while creating new user account", e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public UserAccount login(String email, String password) throws ServiceException {

        try {
            Optional<UserAccount> userAccount = Optional.ofNullable(userAccDAO.getByEmail(email));
            password = passwordEncryptor.encryptPassword(password);
            if (userAccount.isPresent() && userAccount.get().getPassword().equals(password)) {
                LOGGER.info("user with email: Email={} is logged in", email);
                return userAccount.get();
            } else {
                LOGGER.info("incorrect email or password");
                return null;
            }
        } catch (DAOException | NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage(), e);
        }
    }


}
