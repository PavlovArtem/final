package by.pavlov.fos.service.impl;

import by.pavlov.fos.dao.*;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.dto.DriverOfferDto;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.DriverOffer;
import by.pavlov.fos.service.ClientFreightService;
import by.pavlov.fos.service.DriverFreightService;

import java.util.Base64;
import java.util.List;
import java.util.Optional;

public class DriverFreightServiceImpl implements DriverFreightService {


    private final ClientFreightService clientFreightService;
    private final DriverOfferDAO driverOfferDAO;
    private final FreightRequestDAO freightRequestDAO;
    private final TransportDAO transportDAO;
    private final RouteDAO routeDAO;
    private final UserAccDAO userAccDAO;

    public DriverFreightServiceImpl(ClientFreightService clientFreightService, DriverOfferDAO driverOfferDAO, FreightRequestDAO freightRequestDAO, TransportDAO transportDAO, RouteDAO routeDAO, UserAccDAO userAccDAO) {
        this.clientFreightService = clientFreightService;
        this.driverOfferDAO = driverOfferDAO;
        this.freightRequestDAO = freightRequestDAO;
        this.transportDAO = transportDAO;
        this.routeDAO = routeDAO;
        this.userAccDAO = userAccDAO;
    }

    @Override
    public DisplayOfferDto getDriverOfferForRequest(Long userId, Long freightId) throws DAOException {

        Optional<DriverOffer> optionalDriverOffer = driverOfferDAO.findByRequestIdAndUserId(userId, freightId);
        if (optionalDriverOffer.isPresent()) {
            DriverOffer driverOffer = optionalDriverOffer.get();

            return buildDisplayOfferDto(driverOffer);
        }
        return null;

    }

    @Override
    public List<FreightRequestDto> getAllOpenRequests() throws DAOException {
        return clientFreightService.getAllOpenRequests();
    }

    @Override
    public DisplayOfferDto getOfferById(Long offerId) throws DAOException {

        DriverOffer driverOffer = driverOfferDAO.getById(offerId);

        return buildDisplayOfferDto(driverOffer);
    }

    @Override
    public boolean createOffer(DriverOfferDto driverOfferDto) throws DAOException {

        DriverOffer driverOffer = new DriverOffer();
        driverOffer.setPrice(driverOfferDto.getPrice());
        driverOffer.setOfferDescription(driverOfferDto.getOfferDescription());
        driverOffer.setUserAccountId(driverOfferDto.getUserAccountId());
        driverOffer.setFreightRequestId(driverOfferDto.getFreightRequestId());
        driverOffer.setTransportId(driverOfferDto.getTransportId());
        driverOffer.setOfferStatus(DriverOffer.OfferStatus.WAITING);
        driverOfferDAO.save(driverOffer);
        return false;
    }



    private DisplayOfferDto buildDisplayOfferDto(DriverOffer driverOffer) throws DAOException {

        DisplayOfferDto offerDto = new DisplayOfferDto();
        offerDto.setOfferId(driverOffer.getId());
        offerDto.setFreightId(driverOffer.getFreightRequestId());
        offerDto.setPrice(driverOffer.getPrice());
        offerDto.setDescription(driverOffer.getOfferDescription());
        byte[] photo = transportDAO.getById(driverOffer.getTransportId()).getPhoto();
        String base64Image = Base64.getEncoder().encodeToString(photo);
        offerDto.setTransportEncodedPhoto(base64Image);

        return offerDto;

    }

    @Override
    public List<OrderDto> getAllUserOrders(Long userId) {
        return null;
    }


}
