package by.pavlov.fos.service.impl;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dao.TransportDAO;
import by.pavlov.fos.dto.ShowTransportDto;
import by.pavlov.fos.dto.TransportDto;
import by.pavlov.fos.entity.Transport;
import by.pavlov.fos.service.TransportService;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class UserTransportServiceImpl implements TransportService {

    private final TransportDAO transportDAO;

    public UserTransportServiceImpl(TransportDAO transportDAO) {
        this.transportDAO = transportDAO;
    }

    @Override
    public List<ShowTransportDto> findAllUserTransport(Long userId) throws DAOException {

        List<ShowTransportDto> transportDtoList = new ArrayList<>();
        for (Transport transport : transportDAO.findAllByUserId(userId)) {
            transportDtoList.add(transportToDisplayDto(transport));
        }
        return transportDtoList;
    }

    @Override
    public Long createTransport(TransportDto transportDto) throws DAOException {

        transportDAO.save(dtoToTransport(transportDto));
        return null;
    }

    @Override
    public boolean updateTransport(TransportDto transportDto) {
        return false;
    }

    @Override
    public ShowTransportDto getTransport(Long transportId) throws DAOException {

        Transport transport = transportDAO.getById(transportId);
        return transportToDisplayDto(transport);
    }


    private Transport dtoToTransport(TransportDto transportDto) {

        Transport transport = new Transport();
        transport.setName(transportDto.getName());
        transport.setMaxWeight(transportDto.getMaxWeight());
        transport.setMaxVolume(transportDto.getMaxVolume());
        transport.setPhoto(transportDto.getPhoto());
        transport.setDescription(transportDto.getDescription());
        transport.setUserAccountId(transportDto.getUserAccountId());
        return transport;
    }

    private ShowTransportDto transportToDisplayDto(Transport transport) {

        ShowTransportDto transportShowDto = new ShowTransportDto();
        transportShowDto.setId(transport.getId());
        transportShowDto.setName(transport.getName());
        transportShowDto.setMaxWeight(transport.getMaxWeight());
        transportShowDto.setMaxVolume(transport.getMaxVolume());
        String base64Image = Base64.getEncoder().encodeToString(transport.getPhoto());
        transportShowDto.setEncodedPhoto(base64Image);
        transportShowDto.setDescription(transport.getDescription());
        transportShowDto.setUserAccountId(transport.getUserAccountId());
        return transportShowDto;
    }

    @Override
    public boolean deleteTransport(Long transportId) throws DAOException {

        return transportDAO.delete(transportId);
    }
}
