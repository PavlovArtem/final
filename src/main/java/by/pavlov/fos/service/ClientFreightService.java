package by.pavlov.fos.service;

import by.pavlov.fos.dao.DAOException;
import by.pavlov.fos.dto.DisplayOfferDto;
import by.pavlov.fos.dto.DriverOfferDto;
import by.pavlov.fos.dto.FreightRequestDto;
import by.pavlov.fos.dto.OrderDto;
import by.pavlov.fos.entity.DriverOffer;

import java.util.List;

public interface ClientFreightService {

    Long createFreightRequest(FreightRequestDto freightRequestDto) throws DAOException;

    FreightRequestDto getFreightRequest(Long id) throws DAOException;

    List<FreightRequestDto> getAllOpenRequests() throws DAOException;

    List<FreightRequestDto> getAllOpenUserRequests(Long userId) throws DAOException;

    DriverOffer findOfferById(Long offerId) throws DAOException;

    boolean deleteFreightRequest(Long id) throws DAOException;

    List<DisplayOfferDto> getOffers(Long requestId) throws DAOException;

    boolean deleteAllOffersByRequestId(Long id) throws DAOException;


}
