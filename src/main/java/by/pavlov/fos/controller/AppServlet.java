package by.pavlov.fos.controller;

import by.pavlov.fos.application.ApplicationConstants;
import by.pavlov.fos.application.ApplicationContext;
import by.pavlov.fos.command.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig(fileSizeThreshold = 6291456,
        maxFileSize = 10485760L,
        maxRequestSize = 20971520L
)
@WebServlet(urlPatterns = "/", name = "index", loadOnStartup = 0)
public class AppServlet extends HttpServlet {
    
    public static final Logger logger = LogManager.getLogger(AppServlet.class);

    private CommandProvider commandProvider;

    @Override
    public void init() throws ServletException {
        this.commandProvider = ApplicationContext.getInstance().getBean(CommandProvider.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String commandFromRequest = CommandUtil.getCommandFromRequest(req);
        CommandType commandType = CommandType.of(commandFromRequest).orElse(CommandType.INDEX_COMMAND);
        try {
            commandProvider.getCommand(commandType);
            Command command = commandProvider.getCommand(commandType);
            command.execute(req, resp);
        } catch (CommandException ex){
            logger.error(ex.getMessage(),ex);
            resp.sendRedirect(req.getContextPath() + "/" + "?" + ApplicationConstants.COMMAND_PARAM + "=" + CommandType.ERROR_COMMAND);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        doGet(req,resp);

    }
}
